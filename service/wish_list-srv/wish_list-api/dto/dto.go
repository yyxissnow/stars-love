package dto

type WishAccomplishREQ struct {
	ID     uint64 `json:"id"`
	WishID uint64 `json:"wish_id"`
}

type AddWishListREQ struct {
	ID         uint64 `json:"id"`
	LoverID    uint64 `json:"lover_id"`
	Content    string `json:"content"`
	Accomplish bool   `json:"accomplish"`
	Time       int64  `json:"time"`
}

type WishDeleteREQ struct {
	ID     uint64 `json:"id"`
	WishID uint64 `json:"wish_id"`
}

type QueryWishNoREQ struct {
	ID      uint64 `json:"id"`
	LoverID uint64 `json:"lover_id"`
}

type QueryWishNoACK struct {
	ID      uint64 `json:"id"`
	Content string `json:"content"`
	Time    int64  `json:"time"`
}

type QueryWishYesREQ struct {
	ID      uint64 `json:"id"`
	LoverID uint64 `json:"lover_id"`
}

type QueryWishYesACK struct {
	ID      uint64 `json:"id"`
	Content string `json:"content"`
	Time    int64  `json:"time"`
}
