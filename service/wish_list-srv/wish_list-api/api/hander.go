package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"stars/log"
	accountCli "stars/service/account-srv/account-rpc/client"
	accountModel "stars/service/account-srv/dao"
	"stars/service/wish_list-srv/dao"
	"stars/service/wish_list-srv/wish_list-api/dto"
	"stars/util/response"
)

func WishAccomplish(c *gin.Context) {
	var req dto.WishAccomplishREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	var wish dao.WishList
	if err := wish.GetWish(req.WishID); err != nil {
		response.ResError(c, response.ErrGetWish, fmt.Sprintf("%d db get wish err : %v", req.WishID, err))
		return
	}
	wish.SucAccomplish()
	if err := wish.UpdateWish(); err != nil {
		response.ResError(c, response.ErrSaveWish, fmt.Sprintf("%d db save wish err : %v", req.WishID, err))
		return
	}
	log.Logger.Info(fmt.Sprintf("%d save wish accomplish success", req.ID))
	response.ResSuccess(c)
}

func AddWishList(c *gin.Context) {
	var req dto.AddWishListREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	rsp, err := accountCli.GetIDUser(req.ID)
	if err != nil {
		response.ResError(c, response.ErrGetIDUser, fmt.Sprintf("%d account-srv get user err %v", req.ID, errors.Cause(err)))
		return
	}
	if accountModel.ActorType(rsp.Actor) != accountModel.Love {
		response.ResError(c, response.ErrUserNoLover, fmt.Sprintf("%d user no lover", req.ID))
		return
	}
	wish := dao.WishList{
		LoverID:    req.LoverID,
		Content:    req.Content,
		Time:       req.Time,
		Accomplish: req.Accomplish,
	}
	if err = wish.AddWishList(); err != nil {
		response.ResError(c, response.ErrAddWishList, fmt.Sprintf("%d add wish err : %v", req.ID, err))
		return
	}
	log.Logger.Info(fmt.Sprintf("%d add wish success", req.ID))
	response.ResSuccess(c)
}

func WishDelete(c *gin.Context) {
	var req dto.WishDeleteREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	var wish dao.WishList
	if err := wish.GetWish(req.WishID); err != nil {
		response.ResError(c, response.ErrGetWish, fmt.Sprintf("%d db get wish err : %v", req.WishID, err))
		return
	}
	if err := wish.DeleteWish(); err != nil {
		response.ResError(c, response.ErrDeleteWish, fmt.Sprintf("%d db delete wish err : %v", req.WishID, err))
		return
	}
	log.Logger.Info(fmt.Sprintf("%d wish delete success", req.ID))
	response.ResSuccess(c)
}

func QueryNoWish(c *gin.Context) {
	var req dto.QueryWishNoREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	var ack []*dto.QueryWishNoACK
	var wishes []dao.WishList
	if err := dao.QueryNoWish(req.LoverID, &wishes); err != nil {
		response.ResError(c, response.ErrQueryNoWish, fmt.Sprintf("%d db query no wish %v", req.ID, err))
		return
	}
	for _, value := range wishes {
		ack = append(ack, &dto.QueryWishNoACK{
			ID:      value.ID,
			Content: value.Content,
			Time:    value.Time,
		})
	}
	log.Logger.Info(fmt.Sprintf("%d query no wish success", req.ID))
	response.ResSuccessData(c, &ack)
}

func QueryYesWish(c *gin.Context) {
	var req dto.QueryWishNoREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	var ack []*dto.QueryWishYesACK
	var wishes []dao.WishList
	if err := dao.QueryYesWish(req.LoverID, &wishes); err != nil {
		response.ResError(c, response.ErrQueryYesWish, fmt.Sprintf("%d db query yes wish %v", req.ID, err))
		return
	}
	for _, value := range wishes {
		ack = append(ack, &dto.QueryWishYesACK{
			ID:      value.ID,
			Content: value.Content,
			Time:    value.Time,
		})
	}
	log.Logger.Info(fmt.Sprintf("%d query yes wish success", req.ID))
	response.ResSuccessData(c, &ack)
}
