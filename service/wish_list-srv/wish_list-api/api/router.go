package api

import (
	"github.com/gin-gonic/gin"
)

func InitRouter() *gin.Engine {
	r := gin.New()
	v1 := r.Group("/stars")
	{
		v1.POST("/wishList/add_wish_list", AddWishList)
		v1.POST("/wishList/query_no_wish", QueryNoWish)
		v1.POST("/wishList/query_yes_wish", QueryYesWish)
		v1.POST("/wishList/accomplish", WishAccomplish)
		v1.POST("/wishList/delete", WishDelete)
	}
	return r
}
