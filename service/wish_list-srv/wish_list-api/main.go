package main

import (
	"fmt"
	"stars/conn/etcd"
	"stars/util/common"

	"stars/conn/config"
	"stars/conn/db"
	"stars/log"
	account "stars/service/account-srv/account-rpc/client"
	"stars/service/wish_list-srv/wish_list-api/api"
)

func main() {
	log.Init("wishList-api")
	cfg := config.InitConf()
	db.InitDB()
	etcd.Init(cfg.EtcdConf.Address, cfg.EtcdConf.Timeout)
	etcd.RegisterEtcd("wish_list.api", common.GetInIP()+cfg.ServerConf.WishListApiAddr)
	router := api.InitRouter()
	initAllCli()
	if err := router.Run(cfg.ServerConf.WishListApiAddr); err != nil {
		log.Logger.Error(fmt.Sprintf("%s run err : %v", "wish_list-api", err))
		return
	}
}

func initAllCli() {
	account.InitAccountCli()
}
