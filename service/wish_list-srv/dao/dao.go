package dao

import (
	"stars/conn/db"
	"time"
)

type WishList struct {
	ID         uint64     `gorm:"column:id;primary_key;AUTO_INCREMENT;not null" json:"id"`
	CreatedAt  time.Time  `gorm:"column:created_at"`
	UpdatedAt  time.Time  `gorm:"column:updated_at"`
	DeletedAt  *time.Time `gorm:"column:deleted_at" sql:"index"`
	LoverID    uint64     `json:"lover_id"`
	Content    string     `json:"content"`
	Time       int64      `json:"time"`
	Accomplish bool       `json:"accomplish"`
}

func (w *WishList) SucAccomplish() {
	w.Accomplish = true
}

func (w *WishList) AddWishList() error {
	return db.GetMysqlDB().Model(WishList{}).Create(&w).Error
}

func QueryNoWish(id uint64, wishes *[]WishList) error {
	return db.GetMysqlDB().Model(WishList{}).Where("lover_id=? AND accomplish=?", id, false).Find(&wishes).Error
}

func QueryYesWish(id uint64, wishes *[]WishList) error {
	return db.GetMysqlDB().Model(WishList{}).Where("lover_id=? AND accomplish=?", id, true).Find(&wishes).Error
}

func (w *WishList) GetWish(id uint64) error {
	return db.GetMysqlDB().Where("id=?", id).First(&w).Error
}

func (w *WishList) UpdateWish() error {
	return db.GetMysqlDB().Save(&w).Error
}

func (w *WishList) DeleteWish() error {
	return db.GetMysqlDB().Delete(&w).Error
}
