package api

import (
	"github.com/gin-gonic/gin"
)

func InitRouter() *gin.Engine {
	r := gin.New()
	v1 := r.Group("/stars")
	{
		v1.POST("/message/invite_user", InviteUser)
		v1.POST("/message/receive_invite_msg", ReceiveInviteMsg)
	}
	return r
}
