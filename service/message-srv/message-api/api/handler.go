package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"stars/conn/oss"
	"stars/log"
	account "stars/service/account-srv/account-rpc/client"
	user "stars/service/account-srv/dao"
	"stars/service/message-srv/dao"
	"stars/service/message-srv/message-api/dto"
	"stars/util/response"
	"time"
)

func InviteUser(c *gin.Context) {
	var req dto.InviteUserREQ
	err := c.ShouldBindJSON(&req)
	if err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	rspLover, err := account.GetIDUser(req.UserID)
	if err != nil {
		response.ResError(c, response.ErrGetIDUser, fmt.Sprintf("%d  get lover err %v", req.ID, errors.Cause(err)))
		return
	}
	if user.ActorType(rspLover.Actor) != user.Single {
		response.ResError(c, response.ErrUserNoSingle, fmt.Sprintf("%d user no single err", req.ID))
		return
	}
	message := dao.Message{
		UserID:   req.UserID,
		SendUser: req.ID,
		Type:     dao.Receive,
		Content:  req.Content,
		SendTime: time.Now().Unix(),
		Rcv:      false,
	}
	if err = message.SendMessage(); err != nil {
		log.Logger.Error(fmt.Sprintf("%d send msg err", req.ID))
		response.ResError(c, response.ErrSendMsg, fmt.Sprintf("%d send msg err", req.ID))
		return
	}
	log.Logger.Info(fmt.Sprintf("%d lover message success", req.ID))
	response.ResSuccess(c)
}

func ReceiveInviteMsg(c *gin.Context) {
	var req dto.ReceiveInviteMsgREQ
	err := c.ShouldBindJSON(&req)
	if err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}

	var ack []*dto.ReceiveInviteMsgACK
	var messages []dao.Message
	err = dao.FindMessages(req.ID, &messages, dao.Receive)
	if err != nil {
		response.ResError(c, response.ErrFindMsg, fmt.Sprintf("%d  find messages err : %v", req.ID, err))
		return
	}

	for _, value := range messages {
		rsp, err := account.GetIDUser(value.SendUser)
		if err != nil {
			response.ResError(c, response.ErrGetIDUser, fmt.Sprintf("%d  get user err %v", req.ID, errors.Cause(err)))
			return
		}
		ack = append(ack, &dto.ReceiveInviteMsgACK{
			ID:      value.ID,
			UserID:  value.SendUser,
			Name:    rsp.Name,
			Avatar:  oss.DownloadUrl(rsp.Avatar),
			Age:     uint8(rsp.Age),
			Content: value.Content,
			Time:    value.SendTime,
		})
	}
	log.Logger.Info(fmt.Sprintf("%d revive invite mes success", req.ID))
	response.ResSuccessData(c, ack)
}
