package dto

// InviteUser
type InviteUserREQ struct {
	ID      uint64 `json:"id"`
	UserID  uint64 `json:"user_id"`
	Content string `json:"content"`
}

// ReceiveInviteMsg
type ReceiveInviteMsgREQ struct {
	ID uint64 `json:"id"`
}

type ReceiveInviteMsgACK struct {
	ID      uint64 `json:"id"`
	UserID  uint64 `json:"user_id"`
	Name    string `json:"name"`
	Avatar  string `json:"avatar"`
	Age     uint8  `json:"age"`
	Content string `json:"content"`
	Time    int64  `json:"time"`
}
