package main

import (
	"fmt"
	"stars/conn/config"
	"stars/conn/db"
	"stars/conn/etcd"
	"stars/log"
	account "stars/service/account-srv/account-rpc/client"
	"stars/service/message-srv/message-api/api"
	"stars/util/common"
)

func main() {
	log.Init("message-api")
	cfg := config.InitConf()
	db.InitDB()
	etcd.Init(cfg.EtcdConf.Address, cfg.EtcdConf.Timeout)
	etcd.RegisterEtcd("message.api", common.GetInIP())
	router := api.InitRouter()
	initAllCli()

	if err := router.Run(cfg.ServerConf.MessageApiAddr); err != nil {
		log.Logger.Error(fmt.Sprintf("%s run err : %v", "message-api", err))
		return
	}
}

func initAllCli() {
	account.InitAccountCli()
}
