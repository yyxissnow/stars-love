package client

import (
	"context"
	"google.golang.org/grpc"
	"log"
	"stars/conn/config"
	message "stars/service/message-srv/message-rpc/proto"
)

var messageCli message.MessageServerClient

func InitMessageCli() {
	conn, err := grpc.Dial(config.GetCfg().ServerConf.MessageRpcAddr, grpc.WithInsecure())
	if err != nil {
		log.Panicf("message rpc init err:%v", err)
		return
	}
	messageCli = message.NewMessageServerClient(conn)
}

func SendMsg(user, send uint64, types int32, content string) (*message.SendMsgRSP, error) {
	return messageCli.SendMsg(context.TODO(), &message.SendMsgREQ{
		UserId:  user,
		SendId:  send,
		Type:    types,
		Content: content,
	})
}
