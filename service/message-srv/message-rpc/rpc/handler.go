package rpc

import (
	"context"
	"github.com/pkg/errors"
	"stars/service/message-srv/dao"
	message "stars/service/message-srv/message-rpc/proto"
	"stars/util/response"
	"time"
)

type MessageService struct{}

func (h *MessageService) SendMsg(ctx context.Context, req *message.SendMsgREQ) (*message.SendMsgRSP, error) {
	rsp := &message.SendMsgRSP{}
	msg := dao.Message{
		UserID:   req.UserId,
		SendUser: req.SendId,
		Type:     dao.MessagesType(req.Type),
		Content:  req.Content,
		SendTime: time.Now().Unix(),
		Rcv:      false,
	}
	if err := msg.SendMessage(); err != nil {
		rsp.Code = int32(response.ErrRPCCode)
		rsp.Err = err.Error()
		return rsp, errors.Wrap(err, "db send msg err")
	}
	rsp.Code = int32(response.StatusRPCOK)
	return rsp, nil
}
