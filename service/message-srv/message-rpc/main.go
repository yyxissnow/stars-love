package main

import (
	"fmt"
	"google.golang.org/grpc"
	"net"
	"stars/conn/config"
	"stars/conn/db"
	"stars/log"
	message "stars/service/message-srv/message-rpc/proto"
	"stars/service/message-srv/message-rpc/rpc"
)

func main() {
	log.Init("message-rpc")
	cfg := config.InitConf()
	db.InitDB()
	server := grpc.NewServer()
	message.RegisterMessageServerServer(server, new(rpc.MessageService))
	lis, err := net.Listen("tcp", cfg.ServerConf.MessageRpcAddr)
	if err != nil {
		log.Logger.Panic(fmt.Sprintf("net listen err:%v", err))
		return
	}

	log.Logger.Info("message rpc server run")
	if err := server.Serve(lis); err != nil {
		log.Logger.Error(fmt.Sprintf("%s run err : %v", "message-rpc", err))
		return
	}
}
