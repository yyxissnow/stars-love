package dao

import (
	"stars/conn/db"
	"time"
)

type MessagesType int

const (
	Systems MessagesType = iota
	Receive
	Private
)

type Message struct {
	ID        uint64     `gorm:"column:id;primary_key;AUTO_INCREMENT;not null" json:"id"`
	CreatedAt time.Time  `gorm:"column:created_at"`
	UpdatedAt time.Time  `gorm:"column:updated_at"`
	DeletedAt *time.Time `gorm:"column:deleted_at" sql:"index"`
	UserID    uint64     `json:"user_id"` //接收的user
	SendUser  uint64     //发送的user
	Type      MessagesType
	Content   string
	SendTime  int64
	Rcv       bool
}

func (m *Message) SendMessage() error {
	return db.GetMysqlDB().Create(&m).Error
}

func FindMessages(id uint64, msg *[]Message, types MessagesType) error {
	return db.GetMysqlDB().Model(Message{}).Where("user_id=? AND type=?", id, types).Find(&msg).Error
}
