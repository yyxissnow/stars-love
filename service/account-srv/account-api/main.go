package main

import (
	"fmt"
	"stars/conn/config"
	"stars/conn/db"
	"stars/conn/etcd"
	"stars/log"
	"stars/service/account-srv/account-api/api"
	lover "stars/service/lover-srv/lover-rpc/client"
	"stars/util/common"
)

func main() {
	log.Init("account-api")
	cfg := config.InitConf()
	db.InitDB()
	etcd.Init(cfg.EtcdConf.Address, cfg.EtcdConf.Timeout)
	etcd.RegisterEtcd("account.api", common.GetInIP()+cfg.ServerConf.AccountApiAddr)
	router := api.InitRouter()
	initAllCli()

	if err := router.Run(cfg.ServerConf.AccountApiAddr); err != nil {
		log.Logger.Error(fmt.Sprintf("%s run err : %v", "account-api", err))
		return
	}
}

func initAllCli() {
	lover.InitLoverCli()
}
