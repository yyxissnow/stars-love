package dto

// Login
type LoginREQ struct {
	Phone    string `json:"phone"`
	Password string `json:"password"`
}

type LoginACK struct {
	Token  string `json:"token"`
	UserID uint64 `json:"user_id"`
}

// GetInfo
type GetInfoREQ struct {
	ID uint64 `json:"id"`
}

type GetInfoSingleACK struct {
	ID     uint64 `json:"id"`
	Name   string `json:"name"`
	Avatar string `json:"avatar"`
	Phone  string `json:"phone"`
	Gender uint8  `json:"gender"`
	Actor  uint8  `json:"actor"`
}

type GetInfoLoverACK struct {
	ID          uint64 `json:"id"`
	Name        string `json:"name"`
	Avatar      string `json:"avatar"`
	Phone       string `json:"phone"`
	Gender      uint8  `json:"gender"`
	Actor       uint8  `json:"actor"`
	LoveID      uint64 `json:"love_id"`
	LoverID     uint64 `json:"lover_id"`
	Day         uint32 `json:"day"`
	LoverName   string `json:"lover_name"`
	LoverAvatar string `json:"lover_avatar"`
}

// Register
type RegisterREQ struct {
	Name     string `json:"name"`
	Phone    string `json:"phone"`
	Password string `json:"password"`
	Gender   bool   `json:"gender"`
	Age      int    `json:"age"`
	//VerificationCode int    `json:"verification_code"`
}

type RegisterACK struct {
	UserID uint64 `json:"user_id"`
}
