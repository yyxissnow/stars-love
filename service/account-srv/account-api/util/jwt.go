package util

import (
	"github.com/dgrijalva/jwt-go"
	"time"
)

type UserToken struct {
	UserID uint64
	jwt.StandardClaims
}

const TokenExpireDuration = time.Hour * 2

var MySecret = []byte("this is XCY")

func GenToken(id uint64) (string, error) {
	c := UserToken{
		id,
		jwt.StandardClaims{
			Audience:  "",
			ExpiresAt: time.Now().Add(TokenExpireDuration).Unix(),
			Issuer:    "my stars",
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, c)
	return token.SignedString(MySecret)
}
