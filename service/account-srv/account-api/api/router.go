package api

import (
	"github.com/gin-gonic/gin"
)

func InitRouter() *gin.Engine {
	r := gin.New()
	r.Static("/static/", "./bin")
	v1 := r.Group("/stars")
	{
		v1.POST("/account/create", Register)
		v1.POST("/account/login", Login)
		v1.POST("/account/get_info", GetInfo)
	}
	return r
}
