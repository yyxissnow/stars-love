package api

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"gorm.io/gorm"
	"stars/conn/oss"
	"stars/log"
	"stars/service/account-srv/account-api/dto"
	"stars/service/account-srv/account-api/util"
	"stars/service/account-srv/dao"
	lover "stars/service/lover-srv/lover-rpc/client"
	"stars/util/response"
	"time"
)

func Login(c *gin.Context) {
	var req dto.LoginREQ
	err := c.ShouldBindJSON(&req)
	if err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%s  err:bind json err %v", req.Phone, err))
		return
	}
	var user dao.User
	if err := user.Login(req.Phone, req.Password); err != nil {
		log.Logger.Error(fmt.Sprintf("%s account login err : %v ", req.Phone, err))
		response.ResError(c, response.ErrLoginInfo, "")
		return
	}
	token, err := util.GenToken(user.ID)
	if err != nil {
		response.ResError(c, response.ErrGenToken, fmt.Sprintf("%s  err:create token err:%s", req.Phone, err))
		return
	}
	ack := dto.LoginACK{
		UserID: user.ID,
		Token:  token,
	}
	log.Logger.Info(fmt.Sprintf("%d login success", user.ID))
	response.ResSuccessData(c, ack)
}

func GetInfo(c *gin.Context) {
	var req dto.GetInfoREQ
	err := c.ShouldBindJSON(&req)
	if err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	var user dao.User
	if err := user.GetIDUser(req.ID); err != nil {
		response.ResError(c, response.ErrGetIDUser, fmt.Sprintf("%d account rpc err %v", req.ID, err))
		return
	}
	if user.IsLover() {
		rsp, err := lover.GetLoverInfo(user.ID, int32(user.Gender))
		if err != nil {
			response.ResError(c, response.ErrGetLoverInfo, fmt.Sprintf("%d get lover info err : %v", user.ID, errors.Cause(err)))
			return
		}
		var partner dao.User
		err = partner.GetIDUser(rsp.LoverUserId)
		if err != nil {
			response.ResError(c, response.ErrGetIDUser, fmt.Sprintf("%d get id user err : %v", user.ID, errors.Cause(err)))
			return
		}
		ack := dto.GetInfoLoverACK{
			ID:          user.ID,
			Name:        user.Name,
			Avatar:      oss.DownloadUrl(user.Avatar),
			Phone:       user.Phone,
			Gender:      uint8(user.Gender),
			Actor:       uint8(user.Actor),
			LoveID:      rsp.LoverId,
			LoverID:     rsp.LoverUserId,
			Day:         rsp.LoverDay,
			LoverName:   partner.Name,
			LoverAvatar: oss.DownloadUrl(partner.Avatar),
		}
		log.Logger.Info(fmt.Sprintf("%d get love info success", user.ID))
		response.ResSuccessData(c, ack)
	} else {
		ack := dto.GetInfoSingleACK{
			ID:     user.ID,
			Name:   user.Name,
			Avatar: oss.DownloadUrl(user.Avatar),
			Phone:  user.Phone,
			Gender: uint8(user.Gender),
			Actor:  uint8(user.Actor),
		}
		log.Logger.Info(fmt.Sprintf("%d get single info success", user.ID))
		response.ResSuccessData(c, ack)
	}
}

func Register(c *gin.Context) {
	var req dto.RegisterREQ
	jsonStr := c.PostForm("data")
	if jsonStr != "" {
		err := json.Unmarshal([]byte(jsonStr), &req)
		if err != nil {
			response.ResError(c, response.ErrBindJson, fmt.Sprintf("%s  err:bind json err %v", req.Phone, err))
			return
		}
	} else {
		response.ResError(c, response.ErrFormIsNull, fmt.Sprintf("%s  err:jsonStr==nil", req.Phone))
		return
	}
	if req.Phone == "" || req.Password == "" || req.Name == "" {
		response.ResError(c, response.ErrRegisterInfoNull, fmt.Sprintf("%s  err:information err", req.Phone))
		return
	}
	if len(req.Phone) != 11 {
		response.ResError(c, response.ErrPhoneNum, fmt.Sprintf("%s  err:phone number err", req.Phone))
		return
	}

	user := dao.NewUser()
	if err := user.GetPhoneUser(req.Phone); !errors.Is(err, gorm.ErrRecordNotFound) {
		response.ResError(c, response.ErrUserExited, fmt.Sprintf("%s  err:user exist", req.Phone))
		return
	}
	if req.Gender == false {
		user.Gender = dao.Girl
	} else {
		user.Gender = dao.Boy
	}
	user.Name = req.Name
	user.Phone = req.Phone
	user.Actor = dao.Single
	user.Password = req.Password
	user.Age = uint8(req.Age)
	file, err := c.FormFile("ID_Img")
	if err != nil {
		response.ResError(c, response.ErrGetAvatar, fmt.Sprintf("%s get avatar err:  %v", req.Phone, err))
		return
	}
	fileName := fmt.Sprintf("avatarImg/%v_%s.png", time.Now().Format("2006-01-02"), user.Phone)

	err = oss.UploadImg(file, fileName)
	if err != nil {
		response.ResError(c, response.ErrUploadOSS, fmt.Sprintf("%s upload oss avatar %v", req.Phone, err))
		return
	}

	user.Avatar = fileName
	err = user.CreateUser()
	if err != nil {
		response.ResError(c, response.ErrCreateUser, fmt.Sprintf("%s  create user err %v", req.Phone, err))
		return
	}
	log.Logger.Info(user.Phone + "  register_client success")
	response.ResSuccessData(c, dto.RegisterACK{UserID: user.ID})
}
