package dao

import (
	"stars/conn/db"
	articleModel "stars/service/article-srv/dao"
	messageModel "stars/service/message-srv/dao"
	questionModel "stars/service/question-srv/dao"
	reminderModel "stars/service/reminder-srv/dao"
	"time"
)

type ActorType uint8

type GenderType uint8

const (
	Single ActorType = iota
	Love
	SuperAdmin
)

const (
	Girl GenderType = iota //protobuf 忽略0值
	Boy
)

type User struct {
	ID              uint64                    `gorm:"column:id;primary_key;AUTO_INCREMENT;not null" json:"id"`
	CreatedAt       time.Time                 `gorm:"column:created_at"`
	UpdatedAt       time.Time                 `gorm:"column:updated_at"`
	DeletedAt       *time.Time                `gorm:"column:deleted_at" sql:"index"`
	Name            string                    `json:"name" gorm:"column:name;type:varchar(255);not null"`
	Avatar          string                    `json:"avatar"`
	Phone           string                    `json:"phone"`
	Password        string                    `json:"password"`
	Age             uint8                     `json:"age"`
	Gender          GenderType                `json:"gender" gorm:"not null;default:0"`
	Actor           ActorType                 `json:"actor" gorm:"not null;default:0"`
	NotificationCnt uint                      `json:"notification_cnt"`
	Messages        []*messageModel.Message   `json:"messages" gorm:"foreignKey:user_id"`
	Articles        []*articleModel.Article   `json:"articles" gorm:"foreignKey:user_id"`
	Questions       []*questionModel.Question `json:"questions" gorm:"foreignKey:user_id"`
	Reminders       []*reminderModel.Reminder `json:"reminders" gorm:"foreignKey:user_id"` //备忘录
}

func NewUser() *User {
	u := &User{}
	u.Messages = make([]*messageModel.Message, 0)
	u.Articles = make([]*articleModel.Article, 0)
	u.Reminders = make([]*reminderModel.Reminder, 0)
	return u
}

func (u *User) IsLover() bool {
	if u.Actor == Love {
		return true
	} else {
		return false
	}
}

func (u *User) CreateUser() error {
	return db.GetMysqlDB().Create(&u).Error
}

func (u *User) GetIDUser(id uint64) error {
	return db.GetMysqlDB().Where("id=?", id).First(&u).Error
}

func (u *User) Login(phone, passwd string) error {
	return db.GetMysqlDB().Where("phone=? AND password=?", phone, passwd).First(&u).Error
}

func (u *User) GetPhoneUser(phone string) error {
	return db.GetMysqlDB().Where("phone=?", phone).First(&u).Error
}
