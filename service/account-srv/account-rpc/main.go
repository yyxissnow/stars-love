package main

import (
	"fmt"
	"google.golang.org/grpc"
	"net"
	"stars/conn/config"
	"stars/conn/db"
	"stars/log"
	account "stars/service/account-srv/account-rpc/proto"
	"stars/service/account-srv/account-rpc/rpc"
)

func main() {
	log.Init("account-rpc")
	cfg := config.InitConf()
	db.InitDB()
	server := grpc.NewServer()
	account.RegisterAccountServerServer(server, new(rpc.AccountService))
	lis, err := net.Listen("tcp", cfg.ServerConf.AccountRpcAddr)
	if err != nil {
		log.Logger.Panic(fmt.Sprintf("net listen err:%v", err))
		return
	}

	log.Logger.Info("account rpc server run")
	if err := server.Serve(lis); err != nil {
		log.Logger.Error(fmt.Sprintf("%s run err : %v", "account-rpc", err))
		return
	}
}
