package client

import (
	"context"
	"google.golang.org/grpc"
	"log"
	"stars/conn/config"
	account "stars/service/account-srv/account-rpc/proto"
)

var accountCli account.AccountServerClient

func InitAccountCli() {
	conn, err := grpc.Dial(config.GetCfg().ServerConf.AccountRpcAddr, grpc.WithInsecure())
	if err != nil {
		log.Panicf("account rpc init err:%v", err)
		return
	}
	accountCli = account.NewAccountServerClient(conn)
}

func GetIDUser(id uint64) (*account.GetIDUserRSP, error) {
	return accountCli.GetIDUser(context.TODO(), &account.GetIDUserREQ{
		Id: id,
	})
}

func GetPhoneUser(phone string) (*account.GetPhoneUserRSP, error) {
	return accountCli.GetPhoneUser(context.TODO(), &account.GetPhoneUserREQ{
		Phone: phone,
	})
}
