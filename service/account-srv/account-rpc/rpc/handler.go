package rpc

import (
	"context"
	"github.com/pkg/errors"
	account "stars/service/account-srv/account-rpc/proto"
	"stars/service/account-srv/dao"
	"stars/util/response"
)

type AccountService struct{}

func (a *AccountService) GetIDUser(ctx context.Context, req *account.GetIDUserREQ) (*account.GetIDUserRSP, error) {
	var user dao.User
	rsp := &account.GetIDUserRSP{}
	err := user.GetIDUser(req.Id)
	if err != nil {
		rsp.Code = int32(response.ErrRPCCode)
		rsp.Err = err.Error()
		return rsp, errors.Wrap(err, "db get user err")
	}
	rsp.Code = int32(response.StatusRPCOK)
	rsp.Name = user.Name
	rsp.Avatar = user.Avatar
	rsp.Phone = user.Phone
	rsp.Age = uint32(user.Age)
	rsp.Gender = uint32(user.Gender)
	rsp.Actor = uint32(user.Actor)
	return rsp, nil
}

func (a *AccountService) GetPhoneUser(ctx context.Context, req *account.GetPhoneUserREQ) (*account.GetPhoneUserRSP, error) {
	var user dao.User
	rsp := &account.GetPhoneUserRSP{}
	err := user.GetPhoneUser(req.Phone)
	if err != nil {
		rsp.Code = int32(response.ErrRPCCode)
		rsp.Err = err.Error()
		return rsp, errors.Wrap(err, "db get phone user err")
	}
	rsp.Code = int32(response.StatusRPCOK)
	rsp.Name = user.Name
	rsp.Avatar = user.Avatar
	rsp.Phone = user.Phone
	rsp.Age = uint32(user.Age)
	rsp.Gender = uint32(user.Gender)
	rsp.Actor = uint32(user.Actor)
	return rsp, nil
}
