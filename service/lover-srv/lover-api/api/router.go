package api

import (
	"github.com/gin-gonic/gin"
)

func InitRouter() *gin.Engine {
	r := gin.New()
	v1 := r.Group("/stars")
	{
		v1.POST("/lover/agree_invite", AgreeInvite)
		v1.POST("/lover/reject_invite", RejectInvite)
	}
	return r
}
