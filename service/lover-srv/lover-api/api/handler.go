package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"stars/log"
	accountCli "stars/service/account-srv/account-rpc/client"
	accountModel "stars/service/account-srv/dao"
	"stars/service/lover-srv/dao"
	"stars/service/lover-srv/lover-api/dto"
	messageModel "stars/service/message-srv/dao"
	messageCli "stars/service/message-srv/message-rpc/client"
	"stars/util/response"
)

func AgreeInvite(c *gin.Context) {
	var req dto.AgreeInviteREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	rspUser, err := accountCli.GetIDUser(req.ID)
	if err != nil {
		response.ResError(c, response.ErrGetIDUser, fmt.Sprintf("%d account-srv get user err %v", req.ID, errors.Cause(err)))
		return
	}
	rspSend, err := accountCli.GetIDUser(req.SendUserID)
	if err != nil {
		response.ResError(c, response.ErrGetIDUser, fmt.Sprintf("%d account-srv get send user err %v", req.ID, errors.Cause(err)))
		return
	}
	if accountModel.ActorType(rspUser.Actor) != accountModel.Single || accountModel.ActorType(rspSend.Actor) != accountModel.Single {
		response.ResError(c, response.ErrService, fmt.Sprintf("%d user or send no single", req.ID))
		return
	}
	lover := dao.NewLover()
	if accountModel.GenderType(rspUser.Gender) == accountModel.Boy {
		lover.BoyID = req.ID
		lover.GirlID = req.SendUserID
	} else {
		lover.BoyID = req.SendUserID
		lover.GirlID = req.ID
	}
	lover.LoverDay = 1
	err = lover.CreateLover(req.ID, req.SendUserID)
	if err != nil {
		response.ResError(c, response.ErrCreateLover, fmt.Sprintf("%d create lover err : %v", req.ID, err))
		return
	}

	_, err = messageCli.SendMsg(req.SendUserID, 1, int32(messageModel.Systems), "对方同意你的请求，请好好珍惜这段缘分")
	if err != nil {
		response.ResError(c, response.ErrSendMsg, fmt.Sprintf("%d lover send msg err : %v", req.ID, errors.Cause(err)))
		return
	}
	log.Logger.Info(fmt.Sprintf("%d agree inite success", req.ID))
	response.ResSuccess(c)
}

func RejectInvite(c *gin.Context) {
	var req dto.RejectInviteREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	if _, err := messageCli.SendMsg(req.SendUserID, 1, int32(messageModel.Systems), "对方拒绝了你的请求，不要灰心哦"); err != nil {
		response.ResError(c, response.ErrSendMsg, fmt.Sprintf("%d lover send msg err : %v", req.ID, errors.Cause(err)))
		return
	}
	log.Logger.Info(fmt.Sprintf("%d reject invite success", req.ID))
	response.ResSuccess(c)
}
