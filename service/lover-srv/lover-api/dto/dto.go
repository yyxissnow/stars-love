package dto

// AgreeInvite
type AgreeInviteREQ struct {
	ID         uint64 `json:"id"`
	SendUserID uint64 `json:"send_user_id"`
}

type RejectInviteREQ struct {
	ID         uint64 `json:"id"`
	SendUserID uint64 `json:"send_user_id"`
}
