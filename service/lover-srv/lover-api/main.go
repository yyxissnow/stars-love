package main

import (
	"fmt"
	"stars/conn/config"
	"stars/conn/db"
	"stars/conn/etcd"
	"stars/log"
	account "stars/service/account-srv/account-rpc/client"
	"stars/service/lover-srv/lover-api/api"
	message "stars/service/message-srv/message-rpc/client"
	"stars/util/common"
)

func main() {
	log.Init("lover")
	cfg := config.InitConf()
	db.InitDB()
	etcd.Init(cfg.EtcdConf.Address, cfg.EtcdConf.Timeout)
	etcd.RegisterEtcd("lover.api", common.GetInIP()+cfg.ServerConf.LoverApiAddr)
	router := api.InitRouter()
	initAllCli()

	if err := router.Run(cfg.ServerConf.LoverApiAddr); err != nil {
		log.Logger.Error(fmt.Sprintf("%s run err : %v", "lover-api", err))
		return
	}
}

func initAllCli() {
	account.InitAccountCli()
	message.InitMessageCli()
}
