package client

import (
	"context"
	"google.golang.org/grpc"
	"log"
	"stars/conn/config"
	lover "stars/service/lover-srv/lover-rpc/proto"
)

var loverCli lover.LoverServerClient

func InitLoverCli() {
	conn, err := grpc.Dial(config.GetCfg().ServerConf.LoverRpcAddr, grpc.WithInsecure())
	if err != nil {
		log.Panicf("lover rpc init err :%v", err)
		return
	}
	loverCli = lover.NewLoverServerClient(conn)
}

func GetLoverInfo(id uint64, gender int32) (*lover.GetLoverInfoRSP, error) {
	return loverCli.GetLoverInfo(context.TODO(), &lover.GetLoverInfoREQ{
		Id:     id,
		Gender: gender,
	})
}
