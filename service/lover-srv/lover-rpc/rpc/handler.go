package rpc

import (
	"context"
	"github.com/pkg/errors"
	user "stars/service/account-srv/dao"
	"stars/service/lover-srv/dao"
	"stars/service/lover-srv/lover-rpc/proto"
	"stars/util/response"
)

type LoverServer struct{}

func (h *LoverServer) GetLoverInfo(ctx context.Context, req *lover.GetLoverInfoREQ) (*lover.GetLoverInfoRSP, error) {
	var love dao.Lover
	rsp := &lover.GetLoverInfoRSP{}
	var err error
	if user.GenderType(req.Gender) == user.Boy {
		err = love.GetLoverBoyInfo(req.Id)
		rsp.LoverUserId = love.GirlID
	} else {
		err = love.GetLoverGirlInfo(req.Id)
		rsp.LoverUserId = love.BoyID
	}
	if err != nil {
		rsp.Code = int32(response.ErrRPCCode)
		rsp.Err = err.Error()
		return nil, errors.Wrap(err, "db find lover err")
	}
	rsp.Code = int32(response.StatusRPCOK)
	rsp.LoverId = love.ID
	rsp.LoverDay = love.LoverDay
	return rsp, nil
}
