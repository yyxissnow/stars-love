package main

import (
	"fmt"
	"google.golang.org/grpc"
	"net"
	"stars/conn/config"
	"stars/conn/db"
	"stars/log"
	lover "stars/service/lover-srv/lover-rpc/proto"
	"stars/service/lover-srv/lover-rpc/rpc"
)

func main() {
	log.Init("lover-rpc")
	cfg := config.InitConf()
	db.InitDB()
	server := grpc.NewServer()
	lover.RegisterLoverServerServer(server, new(rpc.LoverServer))
	lis, err := net.Listen("tcp", cfg.ServerConf.LoverRpcAddr)
	if err != nil {
		log.Logger.Panic(fmt.Sprintf("net listen err:%v", err))
		return
	}

	log.Logger.Info("account rpc server run")
	if err := server.Serve(lis); err != nil {
		log.Logger.Error(fmt.Sprintf("%s run err : %v", "lover-rpc", err))
		return
	}
}
