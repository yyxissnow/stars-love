package dao

import (
	"gorm.io/gorm"
	"stars/conn/db"
	user "stars/service/account-srv/dao"
	markDayModel "stars/service/mark_day-srv/dao"
	privateChatModel "stars/service/private_chat-srv/dao"
	wishListModel "stars/service/wish_list-srv/dao"
	"time"
)

type Lover struct {
	ID           uint64     `gorm:"column:id;primary_key;AUTO_INCREMENT;not null" json:"id"`
	CreatedAt    time.Time  `gorm:"column:created_at"`
	UpdatedAt    time.Time  `gorm:"column:updated_at"`
	DeletedAt    *time.Time `gorm:"column:deleted_at" sql:"index"`
	BoyID        uint64
	GirlID       uint64
	LoverDay     uint32
	WishLists    []*wishListModel.WishList       `gorm:"foreignKey:LoverID"`
	PrivateChats []*privateChatModel.PrivateChat `gorm:"foreignKey:LoverID"`
	MarkDays     []*markDayModel.MarkDay         `gorm:"foreignKey:LoverID"`
}

func NewLover() *Lover {
	u := &Lover{}
	u.WishLists = make([]*wishListModel.WishList, 0)
	u.PrivateChats = make([]*privateChatModel.PrivateChat, 0)
	u.MarkDays = make([]*markDayModel.MarkDay, 0)
	return u
}

func (l *Lover) CreateLover(receive, send uint64) error {
	return db.GetMysqlDB().Transaction(func(tx *gorm.DB) error {
		if err := tx.Model(user.User{}).Where("id=?", receive).Update("actor", user.Love).Error; err != nil {
			return err
		}

		if err := tx.Model(user.User{}).Where("id=?", send).Update("actor", user.Love).Error; err != nil {
			return err
		}

		if err := tx.Create(&l).Error; err != nil {
			return err
		}

		return nil
	})
}

func (l *Lover) GetLoverBoyInfo(id uint64) error {
	return db.GetMysqlDB().Where("boy_id=?", id).First(l).Error
}

func (l *Lover) GetLoverGirlInfo(id uint64) error {
	return db.GetMysqlDB().Where("girl_id=?", id).First(l).Error
}
