package main

import (
	"fmt"
	"google.golang.org/grpc"
	"net"
	"stars/conn/config"
	"stars/conn/db"
	"stars/log"
	like "stars/service/like-srv/like-rpc/proto"
	"stars/service/like-srv/like-rpc/rpc"
)

func main() {
	log.Init("like-rpc")
	cfg := config.InitConf()
	db.InitDB()
	server := grpc.NewServer()
	like.RegisterLikeServerServer(server, new(rpc.LikeServer))
	lis, err := net.Listen("tcp", cfg.ServerConf.LikeRpcAddr)
	if err != nil {
		log.Logger.Panic(fmt.Sprintf("net listen err:%v", err))
		return
	}

	log.Logger.Info("like rpc server run")
	if err := server.Serve(lis); err != nil {
		log.Logger.Error(fmt.Sprintf("%s run err : %v", "like-rpc", err))
		return
	}
}
