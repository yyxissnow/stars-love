package client

import (
	"context"
	"google.golang.org/grpc"
	"log"
	"stars/conn/config"
	like "stars/service/like-srv/like-rpc/proto"
)

var likeCli like.LikeServerClient

func InitLikeCli() {
	conn, err := grpc.Dial(config.GetCfg().ServerConf.LikeRpcAddr, grpc.WithInsecure())
	if err != nil {
		log.Panicf("like rpc init err:%v", err)
		return
	}
	likeCli = like.NewLikeServerClient(conn)
}

func GetLikeCount(id, likeID uint64, types uint32) (*like.GetLikeCountRSP, error) {
	return likeCli.GetLikeCount(context.TODO(), &like.GetLikeCountREQ{
		Id:     id,
		LikeId: likeID,
		Type:   types,
	})
}

func IsLike(id, likeID uint64, types uint32) (*like.IsLikedRSP, error) {
	return likeCli.IsLiked(context.TODO(), &like.IsLikeREQ{
		Id:      id,
		Type:    types,
		LikedId: likeID,
	})
}
