package rpc

import (
	"context"
	"github.com/pkg/errors"
	"gorm.io/gorm"
	"stars/service/like-srv/dao"
	like "stars/service/like-srv/like-rpc/proto"
	"stars/util/response"
)

type LikeServer struct{}

func (h *LikeServer) IsLiked(ctx context.Context, req *like.IsLikeREQ) (*like.IsLikedRSP, error) {
	rsp := &like.IsLikedRSP{}
	err := dao.IsLiked(req.Id, req.LikedId, req.Type)
	if err == gorm.ErrRecordNotFound {
		rsp.Code = uint32(response.StatusRPCOK)
		rsp.Is = false
		return rsp, nil
	}
	if err != nil {
		rsp.Code = uint32(response.ErrRPCCode)
		rsp.Err = err.Error()
		return rsp, errors.Wrap(err, "db is like err")
	}
	rsp.Code = uint32(response.StatusRPCOK)
	rsp.Is = true
	return rsp, nil
}

func (h *LikeServer) GetLikeCount(ctx context.Context, req *like.GetLikeCountREQ) (*like.GetLikeCountRSP, error) {
	var num int64
	rsp := &like.GetLikeCountRSP{}
	if err := dao.GetLikeCount(req.Type, req.LikeId, &num); err != nil {
		rsp.Code = uint32(response.ErrRPCCode)
		rsp.Err = err.Error()
		return rsp, errors.Wrap(err, "db get like count err")
	}
	rsp.Code = uint32(response.StatusRPCOK)
	rsp.Int = int32(num)
	return rsp, nil
}
