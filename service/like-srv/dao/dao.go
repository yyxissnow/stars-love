package dao

import (
	"errors"
	"stars/conn/db"
	"time"
)

type LikeType uint8

const (
	ArticleLike LikeType = iota
	QuestionLike
	CommentLike
)

type Like struct {
	ID          uint64     `gorm:"column:id;primary_key;AUTO_INCREMENT;not null" json:"id"`
	CreatedAt   time.Time  `gorm:"column:created_at"`
	UpdatedAt   time.Time  `gorm:"column:updated_at"`
	DeletedAt   *time.Time `gorm:"column:deleted_at" sql:"index"`
	Type        LikeType   `json:"type"`
	LikedID     uint64     `json:"liked_id"`      //被赞objectID
	LikedUserID uint64     `json:"liked_user_id"` //被赞内容userID
	UserID      uint64     `json:"user_id"`       //点赞人ID
	Time        int64      `json:"time"`
	IsCheck     bool       `json:"is_check"`
}

func (l *Like) GetLikeContent() (string, error) {
	switch l.Type {
	case ArticleLike:
		//rspArt, err := articleCli.GetArticleContent(l.LikedID)
		//if err != nil {
		//	return "", err
		//}
		//return rspArt.Content, nil
		return "", errors.New("like type err")
	default:
		return "", errors.New("like type err")
	}
}

func GetLikeCount(types uint32, id uint64, num *int64) error {
	return db.GetMysqlDB().Model(Like{}).Where("liked_id=? AND type=?", id, types).Count(num).Error
}

func IsLiked(user uint64, like uint64, types uint32) error {
	return db.GetMysqlDB().Where("type=? AND liked_id=? AND user_id=?", types, like, user).Find(new(Like)).Error
}

func QueryLike(id, likedID uint64, types uint8, like *Like) error {
	return db.GetMysqlDB().Where("liked_id=? AND type=? AND user_id=?", likedID, types, id).Find(&like).Error
}

func (l *Like) DeleteLike() error {
	return db.GetMysqlDB().Delete(&l).Error
}

func (l *Like) CreateLike() error {
	return db.GetMysqlDB().Model(Like{}).Create(&l).Error
}

func GetUserLike(id uint64, types uint32, likes *[]Like) error {
	return db.GetMysqlDB().Where("type=? AND liked_user_id", types, id).Find(&likes).Error
}

func GetUserLimitLike(id uint64, page, size int, likes *[]Like) error {
	return db.GetMysqlDB().Where("liked_user_id=?", id).Order("id desc").Limit(size).Offset((page - 1) * size).Find(&likes).Error
}
