package main

import (
	"fmt"
	"main.go/conn/config"
	"main.go/conn/db"
	"main.go/conn/etcd"
	"main.go/log"
	account "main.go/service/account-srv/account-rpc/client"
	article "main.go/service/article-srv/article-rpc/client"
	"main.go/service/like-srv/like-api/api"
	"main.go/util/common"
)

func main() {
	log.Init("like-api")
	config.InitConf()
	cfg := config.GetCfg()
	db.InitDB()
	etcd.Init(cfg.EtcdConf.Address, cfg.EtcdConf.Timeout)
	etcd.RegisterEtcd("like.api", common.GetInIP()+"")
	router := api.InitRouter()

	initAllCli()

	if err := router.Run(cfg.ServerConf.LikeApiAddr); err != nil {
		log.Logger.Error(fmt.Sprintf("%s run err : %v", "like-api", err))
		return
	}
}

func initAllCli() {
	account.InitAccountCli()
	article.InitArticleCli()
}
