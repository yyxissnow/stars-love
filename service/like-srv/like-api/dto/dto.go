package dto

type AddLikeREQ struct {
	ID          uint64 `json:"id"`
	ObjectID    uint64 `json:"object_id"`
	ObjectType  uint8  `json:"object_type"`
	LikedUserID uint64 `json:"liked_user_id"`
}

type GetLikeREQ struct {
	ID   uint64 `json:"id"`
	Page uint8  `json:"page"`
	Size uint8  `json:"size"`
}

type GetLikeAck struct {
	ID              uint64
	LikeType        uint8
	LikedUser       uint64
	LikedUserAvatar string
	LikedUserName   string
	LikedContent    string
	Time            int64
	IsCheck         bool
}
