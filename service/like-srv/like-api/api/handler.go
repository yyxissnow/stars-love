package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"stars/log"
	accountCli "stars/service/account-srv/account-rpc/client"
	"stars/service/like-srv/dao"
	"stars/service/like-srv/like-api/dto"
	"stars/util/response"
	"time"
)

func AddLike(c *gin.Context) {
	var req dto.AddLikeREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	var liked dao.Like
	if err := dao.QueryLike(req.ID, req.ObjectID, req.ObjectType, &liked); err != gorm.ErrRecordNotFound {
		if err = liked.DeleteLike(); err != nil {
			response.ResError(c, response.ErrDeleteLike, fmt.Sprintf("%d  err:delete like object err %v", req.ID, err))
			return
		}
		log.Logger.Info(fmt.Sprintf("%d cancel like success", req.ID))
		response.ResSuccess(c)
		return
	}
	liked.LikedID = req.ObjectID
	liked.UserID = req.ID
	liked.Type = dao.LikeType(req.ObjectType)
	liked.LikedUserID = req.LikedUserID
	liked.Time = time.Now().Unix()
	liked.IsCheck = false
	if err := liked.CreateLike(); err != nil {
		response.ResError(c, response.ErrCreateLike, fmt.Sprintf("%d  err:create like object err %v", req.ID, err))
		return
	}
	log.Logger.Info(fmt.Sprintf("%d add like success", req.ID))
	response.ResSuccess(c)
}

func GetLike(c *gin.Context) {
	var req dto.GetLikeREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	var likes []dao.Like
	var ack []dto.GetLikeAck
	if err := dao.GetUserLimitLike(req.ID, int(req.Page), int(req.Size), &likes); err != nil {
		response.ResError(c, response.ErrGetLimitLike, fmt.Sprintf("%d  get limit like err %v", req.ID, err))
		return
	}
	for _, like := range likes {
		content, err := like.GetLikeContent()
		if err != nil {
			response.ResError(c, response.ErrGetArticleContent, fmt.Sprintf("%d get article content err %v", req.ID, err))
			return
		}
		rspAcc, err := accountCli.GetIDUser(like.UserID)
		if err != nil {
			response.ResError(c, response.ErrGetIDUser, fmt.Sprintf("%d get like user err %v", req.ID, err))
			return
		}
		ack = append(ack, dto.GetLikeAck{
			ID:              like.ID,
			LikeType:        uint8(like.Type),
			LikedUser:       like.UserID,
			LikedUserAvatar: rspAcc.Avatar,
			LikedUserName:   rspAcc.Name,
			LikedContent:    content,
			Time:            like.Time,
			IsCheck:         like.IsCheck,
		})
	}
	log.Logger.Info(fmt.Sprintf("%d get like success", req.ID))
	response.ResSuccessData(c, ack)
}
