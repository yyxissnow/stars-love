package api

import (
	"github.com/gin-gonic/gin"
)

func InitRouter() *gin.Engine {
	r := gin.New()
	r.Static("/static/", "./bin")
	v1 := r.Group("/stars")
	{
		v1.POST("/like/add", AddLike)
		v1.POST("/like/get", GetLike)
	}
	return r
}
