package dto

type AddPrivateChatREQ struct {
	ID      uint64 `json:"id"`
	LoverID uint64 `json:"lover_id"`
	Content string `json:"content"`
}

type QueryPrivateChatREQ struct {
	ID        uint64 `json:"id"`
	LoverUser uint64 `json:"lover_user"`
	LoverID   uint64 `json:"lover_id"`
}

type QueryPrivateChatACK struct {
	ID      uint64 `json:"id"`
	UserID  uint64 `json:"user_id"`
	Name    string `json:"name"`
	Avatar  string `json:"avatar"`
	Content string `json:"content"`
	Time    int64  `json:"time"`
}
