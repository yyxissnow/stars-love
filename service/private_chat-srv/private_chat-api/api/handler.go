package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"stars/log"
	accountCli "stars/service/account-srv/account-rpc/client"
	accountModel "stars/service/account-srv/dao"
	"stars/service/private_chat-srv/dao"
	"stars/service/private_chat-srv/private_chat-api/dto"
	"stars/util/response"
	"time"
)

func AddPrivateChat(c *gin.Context) {
	var req dto.AddPrivateChatREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	rsp, err := accountCli.GetIDUser(req.ID)
	if err != nil {
		response.ResError(c, response.ErrGetIDUser, fmt.Sprintf("%d account-srv get user err %v", req.ID, errors.Cause(err)))
		return
	}
	if accountModel.ActorType(rsp.Actor) != accountModel.Love {
		response.ResError(c, response.ErrUserNoLover, fmt.Sprintf("%d user no lover", req.ID))
		return
	}
	private := dao.PrivateChat{
		LoverID: req.LoverID,
		UserID:  req.ID,
		Content: req.Content,
		Time:    time.Now().Unix(),
	}
	if err = private.AddPrivateChat(); err != nil {
		response.ResError(c, response.ErrAddPrivateChat, fmt.Sprintf("%d  save private chat fail", req.ID))
		return
	}
	log.Logger.Info(fmt.Sprintf("%d  add private success", req.ID))
	response.ResSuccess(c)
}

func QueryPrivateChat(c *gin.Context) {
	var req dto.QueryPrivateChatREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	rspUser, err := accountCli.GetIDUser(req.ID)
	if err != nil {
		response.ResError(c, response.ErrGetIDUser, fmt.Sprintf("%d get user err %v", req.ID, errors.Cause(err)))
		return
	}
	if accountModel.ActorType(rspUser.Actor) != accountModel.Love {
		response.ResError(c, response.ErrUserNoLover, fmt.Sprintf("%d user no lover", req.ID))
		return
	}
	rspLover, err := accountCli.GetIDUser(req.LoverUser)
	if err != nil {
		response.ResError(c, response.ErrGetIDUser, fmt.Sprintf("%d get user err %v", req.ID, errors.Cause(err)))
		return
	}
	var privates []dao.PrivateChat
	var ack []dto.QueryPrivateChatACK
	if err = dao.QueryPrivateChat(req.LoverID, &privates); err != nil {
		response.ResError(c, response.ErrQueryPrivateChat, fmt.Sprintf("%d db query private chat err %v", req.ID, errors.Cause(err)))
		return
	}
	for _, value := range privates {
		if value.UserID == req.ID {
			ack = append(ack, dto.QueryPrivateChatACK{
				ID:      value.ID,
				UserID:  value.UserID,
				Name:    rspUser.Name,
				Avatar:  rspUser.Avatar,
				Content: value.Content,
				Time:    value.Time,
			})
		} else {
			ack = append(ack, dto.QueryPrivateChatACK{
				ID:      value.ID,
				UserID:  value.UserID,
				Name:    rspLover.Name,
				Avatar:  rspLover.Avatar,
				Content: value.Content,
				Time:    value.Time,
			})
		}
	}
	log.Logger.Info(fmt.Sprintf("%d  query no privateChat success", req.ID))
	response.ResSuccessData(c, &ack)
}
