package api

import (
	"github.com/gin-gonic/gin"
)

func InitRouter() *gin.Engine {
	r := gin.New()
	v1 := r.Group("/stars")
	{
		v1.POST("/privateChat/add", AddPrivateChat)
		v1.POST("/privateChat/query", QueryPrivateChat)
		v1.GET("/privateChat/chat", LoverChat)
	}
	return r
}
