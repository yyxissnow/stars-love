package api

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"github.com/pkg/errors"
	"gopkg.in/fatih/set.v0"
	"net/http"
	"stars/service/account-srv/account-rpc/client"
	"stars/service/private_chat-srv/dao"
	"stars/util/response"
	"strconv"
	"sync"
)

type ChatManagement struct {
	ClientMap map[uint64]*Node
	RwLocker  sync.RWMutex

	HeartMap map[uint64]int64
}

type Node struct {
	Conn      *websocket.Conn
	DataQueue chan []byte
	GroupSets set.Interface
}

var clientManage ChatManagement

func InitChatMsg() {
	clientManage = ChatManagement{ClientMap: make(map[uint64]*Node)}
}

func LoverChat(c *gin.Context) {
	query := c.Request.URL.Query()
	id, err := strconv.Atoi(query.Get("id"))
	if err != nil {
		response.ResError(c, response.ErrStringToInt, fmt.Sprintf("%d strconv string err : %v", id, err))
		return
	}
	isValida := true
	_, err = client.GetIDUser(uint64(id))
	if err != nil {
		response.ResError(c, response.ErrGetIDUser, fmt.Sprintf("%d account-srv get user err %v", id, errors.Cause(err)))
		return
	}
	conn, err := (&websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			return isValida
		},
	}).Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		response.ResError(c, response.ErrUpdateWebSocket, fmt.Sprintf("%d update websocket %v", id, err))
		return
	}

	node := &Node{
		Conn:      conn,
		DataQueue: make(chan []byte, 50),
		GroupSets: set.New(set.ThreadSafe),
	}
	clientManage.RwLocker.Lock()
	clientManage.ClientMap[uint64(id)] = node
	clientManage.RwLocker.Unlock()
	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		err = sendProc(ctx, node)
		if err != nil {
			response.ResError(c, response.ErrChatSendMsg, fmt.Sprintf("%d chat send message %v", id, err))
			cancel()
		}
	}()
	go func() {
		err = recvProc(ctx, node)
		if err != nil {
			response.ResError(c, response.ErrChatRecvMsg, fmt.Sprintf("%d chat receive message %v", id, errors.Cause(err)))
			cancel()
		}
	}()
}

//ws发送协程
func sendProc(ctx context.Context, node *Node) error {
	fmt.Println("sendProc success")
	for {
		select {
		case <-ctx.Done():
			return nil

		case data := <-node.DataQueue:
			err := node.Conn.WriteMessage(websocket.TextMessage, data)
			if err != nil {
				return err
			}
		}
	}
}

//ws接收协程
func recvProc(ctx context.Context, node *Node) error {
	fmt.Println("recvProc success")
	for {
		select {
		case <-ctx.Done():
			return nil

		default:
			_, data, err := node.Conn.ReadMessage()
			if err != nil {
				return errors.Wrap(err, "conn websocket read err")
			}
			err = dispatch(data)
			if err != nil {
				return err
			}
		}
	}
}

//后端调度逻辑处理
func dispatch(data []byte) error {
	msg := dao.MessageChat{}
	err := json.Unmarshal(data, &msg)
	fmt.Println(msg)
	if err != nil {
		return errors.Wrap(err, "unmarshal json err")
	}
	sendMsg(msg.RecvUserID, data)
	return nil
}

func sendMsg(userId uint64, msg []byte) {
	clientManage.RwLocker.RLock()
	node, ok := clientManage.ClientMap[userId]
	clientManage.RwLocker.RUnlock()
	if ok {
		node.DataQueue <- msg
	}
}
