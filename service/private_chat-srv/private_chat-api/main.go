package main

import (
	"fmt"
	"main.go/conn/config"
	"main.go/conn/db"
	"main.go/conn/etcd"
	"main.go/log"
	account "main.go/service/account-srv/account-rpc/client"
	"main.go/service/private_chat-srv/private_chat-api/api"
	"main.go/util/common"
)

func main() {
	log.Init("private_chat-api")
	config.InitConf()
	cfg := config.GetCfg()
	db.InitDB()
	etcd.Init(cfg.EtcdConf.Address, cfg.EtcdConf.Timeout)
	etcd.RegisterEtcd("private_chat.api", common.GetInIP()+cfg.ServerConf.PrivateChatApiAddr)
	router := api.InitRouter()
	initAllCli()

	if err := router.Run(cfg.ServerConf.PrivateChatApiAddr); err != nil {
		log.Logger.Error(fmt.Sprintf("%s run err : %v", "private_chat-api", err))
		return
	}
}

func initAllCli() {
	account.InitAccountCli()
	api.InitChatMsg()
}
