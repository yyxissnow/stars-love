package dao

import (
	"stars/conn/db"
	"time"
)

type PrivateChat struct {
	ID        uint64     `gorm:"column:id;primary_key;AUTO_INCREMENT;not null" json:"id"`
	CreatedAt time.Time  `gorm:"column:created_at"`
	UpdatedAt time.Time  `gorm:"column:updated_at"`
	DeletedAt *time.Time `gorm:"column:deleted_at" sql:"index"`
	LoverID   uint64
	UserID    uint64
	Content   string
	Time      int64
}

func (p *PrivateChat) AddPrivateChat() error {
	return db.GetMysqlDB().Create(&p).Error
}

func QueryPrivateChat(id uint64, chats *[]PrivateChat) error {
	return db.GetMysqlDB().Where("lover_id=?", id).Find(&chats).Error
}

type Media int

const (
	Txt Media = iota
	Picture
	Audio
)

type MessageChat struct {
	ID           uint64 `json:"id"`
	UserID       uint64 `json:"user_id"`
	SendUserName string `json:"send_user_name"`
	RecvUserID   uint64 `json:"recv_user_id"`
	RecvUserName string `json:"recv_user_name"`
	Media        Media  `json:"media"`
	Content      string `json:"content"`
	Pic          string `json:"pic"`
	URL          string `json:"url"`
	Memo         string `json:"memo"` //描述
	Amount       int    `json:"amount"`
}
