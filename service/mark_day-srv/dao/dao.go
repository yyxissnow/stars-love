package dao

import (
	"stars/conn/db"
	"time"
)

type MarkDayType int

const (
	Diary  MarkDayType = iota //纪念日
	Remind                    //提醒日
)

type MarkDay struct {
	ID          uint64     `gorm:"column:id;primary_key;AUTO_INCREMENT;not null" json:"id"`
	CreatedAt   time.Time  `gorm:"column:created_at"`
	UpdatedAt   time.Time  `gorm:"column:updated_at"`
	DeletedAt   *time.Time `gorm:"column:deleted_at" sql:"index"`
	LoverID     uint64
	Type        MarkDayType
	Content     string
	Time        int64
	ReminderDay int16 //纪念天数
	Countdown   int16 //倒计时
}

func (m *MarkDay) AddDiary() error {
	return db.GetMysqlDB().Create(&m).Error
}

func (m *MarkDay) AddReminder() error {
	return db.GetMysqlDB().Create(&m).Error
}

func QueryMarkDay(id uint64, types int, days *[]MarkDay) error {
	return db.GetMysqlDB().Where("lover_id=? AND type=?", id, types).Find(&days).Error
}
