package api

import (
	"github.com/gin-gonic/gin"
)

func InitRouter() *gin.Engine {
	r := gin.New()
	v1 := r.Group("/stars")
	{
		v1.POST("/markDay/add_diary", AddDiary)
		v1.POST("/markDay/add_remind", AddRemind)
		v1.POST("/markDay/query_diary", QueryDiaryDay)
		v1.POST("/markDay/query_reminder", QueryReminderDay)
	}
	return r
}
