package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"stars/log"
	accountCli "stars/service/account-srv/account-rpc/client"
	accountModel "stars/service/account-srv/dao"
	"stars/service/mark_day-srv/dao"
	"stars/service/mark_day-srv/mark_day-api/dto"
	"stars/util/response"
	"time"
)

func AddDiary(c *gin.Context) {
	var req dto.AddDiaryREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	rsp, err := accountCli.GetIDUser(req.ID)
	if err != nil {
		response.ResError(c, response.ErrGetIDUser, fmt.Sprintf("%d account-srv get user err %v", req.ID, errors.Cause(err)))
		return
	}
	if accountModel.ActorType(rsp.Actor) != accountModel.Love {
		response.ResError(c, response.ErrUserNoLover, fmt.Sprintf("%d user no lover", req.ID))
		return
	}
	mark := dao.MarkDay{
		LoverID:     req.LoverID,
		Content:     req.Content,
		Type:        dao.Diary,
		Time:        time.Now().Unix(),
		ReminderDay: req.Day,
	}
	if err = mark.AddDiary(); err != nil {
		response.ResError(c, response.ErrAddDiaryMarkDay, fmt.Sprintf("%d add diary mark day err : %v", req.ID, err))
		return
	}
	log.Logger.Info(fmt.Sprintf("%d add mark day diary success", req.ID))
	response.ResSuccess(c)
}

func AddRemind(c *gin.Context) {
	var req dto.AddRemindREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	rsp, err := accountCli.GetIDUser(req.ID)
	if err != nil {
		response.ResError(c, response.ErrGetIDUser, fmt.Sprintf("%d account-srv get user err %v", req.ID, errors.Cause(err)))
		return
	}
	if accountModel.ActorType(rsp.Actor) != accountModel.Love {
		response.ResError(c, response.ErrUserNoLover, fmt.Sprintf("%d user no lover", req.ID))
		return
	}
	mark := dao.MarkDay{
		LoverID:   req.LoverID,
		Content:   req.Content,
		Type:      dao.Remind,
		Time:      time.Now().Unix(),
		Countdown: req.Countdown,
	}
	if err = mark.AddReminder(); err != nil {
		response.ResError(c, response.ErrAddReminderMarkDay, fmt.Sprintf("%d add reminder mark day err : %v", req.ID, err))
		return
	}
	log.Logger.Info(fmt.Sprintf("%d add mark day reminder success", req.ID))
	response.ResSuccess(c)
}

func QueryDiaryDay(c *gin.Context) {
	var req dto.QueryDiaryDayREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	var ack []*dto.QueryDiaryDayACK
	var diaries []dao.MarkDay

	if err := dao.QueryMarkDay(req.LoverID, int(dao.Diary), &diaries); err != nil {
		response.ResError(c, response.ErrQueryDiaryDay, fmt.Sprintf("%d db query diary mark day %v", req.ID, err))
		return
	}
	for _, value := range diaries {
		ack = append(ack, &dto.QueryDiaryDayACK{
			ID:          value.ID,
			Content:     value.Content,
			ReminderDay: value.ReminderDay,
			Time:        value.Time,
		})
	}
	log.Logger.Info(fmt.Sprintf("%d query diary day success", req.ID))
	response.ResSuccessData(c, &ack)
}

func QueryReminderDay(c *gin.Context) {
	var req dto.QueryReminderDayREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	var ack []*dto.QueryReminderDayACK
	var diaries []dao.MarkDay
	if err := dao.QueryMarkDay(req.LoverID, int(dao.Remind), &diaries); err != nil {
		response.ResError(c, response.ErrQueryReminderDay, fmt.Sprintf("%d db query reminder mark day %v", req.ID, err))
		return
	}
	for _, value := range diaries {
		ack = append(ack, &dto.QueryReminderDayACK{
			ID:        value.ID,
			Content:   value.Content,
			Countdown: value.Countdown,
			Time:      value.Time,
		})
	}
	log.Logger.Info(fmt.Sprintf("%d query reminder day success", req.ID))
	response.ResSuccessData(c, &ack)
}
