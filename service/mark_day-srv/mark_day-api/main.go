package main

import (
	"fmt"
	"stars/conn/config"
	"stars/conn/db"
	"stars/conn/etcd"
	"stars/log"
	account "stars/service/account-srv/account-rpc/client"
	"stars/service/mark_day-srv/mark_day-api/api"
	"stars/util/common"
)

func main() {
	log.Init("mark_day-api")
	cfg := config.InitConf()
	etcd.Init(cfg.EtcdConf.Address, cfg.EtcdConf.Timeout)
	etcd.RegisterEtcd("mark_day.api", common.GetInIP()+cfg.ServerConf.MarkDayApiAddr)
	db.InitDB()
	router := api.InitRouter()
	initAllCli()

	if err := router.Run(cfg.ServerConf.MarkDayApiAddr); err != nil {
		log.Logger.Error(fmt.Sprintf("%s run err : %v", "mark_day-api", err))
		return
	}
}

func initAllCli() {
	account.InitAccountCli()
}
