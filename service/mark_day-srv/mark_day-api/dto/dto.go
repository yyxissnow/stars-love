package dto

type AddDiaryREQ struct {
	ID      uint64 `json:"id"`
	LoverID uint64 `json:"lover_id"`
	Content string `json:"content"`
	Day     int16  `json:"day"`
}

type AddRemindREQ struct {
	ID        uint64 `json:"id"`
	LoverID   uint64 `json:"lover_id"`
	Content   string `json:"content"`
	Countdown int16  `json:"countdown"`
}

type QueryDiaryDayREQ struct {
	ID      uint64 `json:"id"`
	LoverID uint64 `json:"lover_id"`
}

type QueryDiaryDayACK struct {
	ID          uint64 `json:"id"`
	Content     string `json:"content"`
	ReminderDay int16  `json:"reminder_day"`
	Time        int64  `json:"time"`
}

type QueryReminderDayREQ struct {
	ID      uint64 `json:"id"`
	LoverID uint64 `json:"lover_id"`
}

type QueryReminderDayACK struct {
	ID        uint64 `json:"id"`
	Content   string `json:"content"`
	Countdown int16  `json:"countdown"`
	Time      int64  `json:"time"`
}
