package rpc

import (
	"context"
	"github.com/pkg/errors"
	comment "stars/service/comment-srv/comment-rpc/proto"
	"stars/service/comment-srv/dao"
	"stars/util/response"
)

type CommentServer struct{}

func (h *CommentServer) GetCommentCount(ctx context.Context, req *comment.GetCommentCountREQ) (*comment.GetCommentCountRSP, error) {
	var num int64
	rsp := &comment.GetCommentCountRSP{}
	err := dao.GetCommentCount(req.Type, req.CommentId, &num)
	if err != nil {
		rsp.Code = uint32(response.ErrRPCCode)
		rsp.Err = err.Error()
		return rsp, errors.Wrap(err, "db get comment count err")
	}
	rsp.Code = uint32(response.StatusRPCOK)
	rsp.Int = int32(num)
	return rsp, nil
}
