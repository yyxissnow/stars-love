package client

import (
	"context"
	"google.golang.org/grpc"
	"log"
	"stars/conn/config"
	comment "stars/service/comment-srv/comment-rpc/proto"
)

var commentCli comment.CommentServerClient

func InitCommentCli() {
	conn, err := grpc.Dial(config.GetCfg().ServerConf.CommentRpcAddr, grpc.WithInsecure())
	if err != nil {
		log.Panicf("comment rpc init err:%v", err)
		return
	}
	commentCli = comment.NewCommentServerClient(conn)
}

func GetCommentCount(id, commentID uint64, types uint32) (*comment.GetCommentCountRSP, error) {
	return commentCli.GetCommentCount(context.TODO(), &comment.GetCommentCountREQ{
		Id:        id,
		CommentId: commentID,
		Type:      types,
	})
}
