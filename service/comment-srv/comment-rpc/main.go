package main

import (
	"fmt"
	"google.golang.org/grpc"
	"net"
	"stars/conn/config"
	"stars/conn/db"
	"stars/log"
	account "stars/service/account-srv/account-rpc/client"
	comment "stars/service/comment-srv/comment-rpc/proto"
	"stars/service/comment-srv/comment-rpc/rpc"
)

func main() {
	log.Init("comment-rpc")
	cfg := config.InitConf()
	db.InitDB()
	server := grpc.NewServer()
	comment.RegisterCommentServerServer(server, new(rpc.CommentServer))
	initAllCli()
	lis, err := net.Listen("tcp", cfg.ServerConf.CommentRpcAddr)
	if err != nil {
		log.Logger.Panic(fmt.Sprintf("net listen err:%v", err))
		return
	}

	log.Logger.Info("comment rpc server run")
	if err := server.Serve(lis); err != nil {
		log.Logger.Error(fmt.Sprintf("%s run err : %v", "comment-rpc", err))
		return
	}
}

func initAllCli() {
	account.InitAccountCli()
}
