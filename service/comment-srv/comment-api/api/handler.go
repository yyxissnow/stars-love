package api

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"stars/conn/db"
	redisCli "stars/conn/redis"
	"stars/log"
	accountCli "stars/service/account-srv/account-rpc/client"
	"stars/service/comment-srv/comment-api/dto"
	"stars/service/comment-srv/dao"
	"stars/util/response"
	"strconv"
	"time"
)

func AddComment(c *gin.Context) {
	var req dto.AddCommentREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	commented := dao.Comment{
		Type:        dao.CommentType(req.ObjectType),
		CommentedID: req.ObjectID,
		ParentID:    req.ParentID,
		UserID:      req.ID,
		Time:        time.Now().Unix(),
		Content:     req.Content,
		//Likes:       make([]*likeModel.Like, 0),
	}
	if req.ParentID != 0 {
		if err := db.GetMysqlDB().Transaction(func(tx *gorm.DB) error {
			if err := dao.AddCommentFloor(req.ParentID); err != nil {
				return err
			}
			if err := commented.CreateComment(); err != nil {
				return err
			}
			return nil
		}); err != nil {
			response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d add comment db transaction err %v", req.ID, err))
			return
		}
		log.Logger.Info(fmt.Sprintf("%d add comment success", req.ID))
		response.ResSuccess(c)
	}
	if err := commented.CreateComment(); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d add comment err %v", req.ID, err))
		return
	}
	log.Logger.Info(fmt.Sprintf("%d add comment success", req.ID))
	response.ResSuccess(c)
}

func GetComment(c *gin.Context) {
	var req dto.GetCommentREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	var comments []dao.Comment
	var ack []dto.GetCommentACK
	info, err := redisCli.GetRedisCli().Get(context.TODO(), "comment_id_"+strconv.Itoa(int(req.ObjectID))).Bytes()
	if err == nil { //找到了redis中的数据
		err = json.Unmarshal(info, &ack)
		log.Logger.Info(fmt.Sprintf("%d get root comment redis success", req.ID))
		response.ResSuccessData(c, ack)
		return
	}
	if req.IsRoot == true {
		err = dao.QueryRootComment(req.ObjectID, req.ObjectType, req.Size, req.Page, &comments)
		if err != nil {
			response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d query root comment db err %v", req.ID, err))
			return
		}
	} else {
		err = dao.QueryChildComment(req.ObjectID, req.ObjectType, req.Size, req.Page, &comments)
		if err != nil {
			response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d query child comment db err %v", req.ID, err))
			return
		}
	}
	for _, v := range comments {
		rspUser, err := accountCli.GetIDUser(v.UserID)
		if err != nil {
			response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d get comment user db err %v", req.ID, err))
			return
		}
		ack = append(ack, dto.GetCommentACK{
			ID:         v.ID,
			UserID:     v.UserID,
			UserName:   rspUser.Name,
			UserAvatar: rspUser.Avatar,
			Time:       v.Time,
			Content:    v.Content,
			//Like:       uint64(v.Likes),
			Floor: v.Floor,
		})
	}
	data, _ := json.Marshal(ack)
	if err = redisCli.GetRedisCli().Set(context.TODO(), "comment_id_"+strconv.Itoa(int(req.ObjectID)), string(data), time.Second*5).Err(); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d save redis err %v", req.ID, err))
		return
	}
	log.Logger.Info(fmt.Sprintf("%d get comment mysql success", req.ID))
	response.ResSuccessData(c, ack)
}
