package api

import (
	"github.com/gin-gonic/gin"
)

func InitRouter() *gin.Engine {
	r := gin.New()
	r.Static("/static/", "./bin")
	v1 := r.Group("/stars")
	{
		v1.POST("/comment/add", AddComment)
		v1.POST("/comment/get", GetComment)
	}
	return r
}
