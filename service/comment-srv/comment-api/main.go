package main

import (
	"fmt"
	"stars/conn/config"
	"stars/conn/db"
	"stars/conn/etcd"
	"stars/conn/redis"
	"stars/log"
	account "stars/service/account-srv/account-rpc/client"
	"stars/service/comment-srv/comment-api/api"
	"stars/util/common"
)

func main() {
	log.Init("comment-api")
	cfg := config.InitConf()
	db.InitDB()
	etcd.Init(cfg.EtcdConf.Address, cfg.EtcdConf.Timeout)
	etcd.RegisterEtcd("comment.api", common.GetInIP()+cfg.ServerConf.CommentApiAddr)
	redis.InitRedisCli(config.GetCfg().RedisConf)
	router := api.InitRouter()
	initAllCli()

	if err := router.Run(cfg.ServerConf.CommentApiAddr); err != nil {
		log.Logger.Error(fmt.Sprintf("%s run err : %v", "comment-api", err))
		return
	}
}

func initAllCli() {
	account.InitAccountCli()
}
