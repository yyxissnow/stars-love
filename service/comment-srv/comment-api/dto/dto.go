package dto

type AddCommentREQ struct {
	ID         uint64 `json:"id"`
	ObjectID   uint64 `json:"object_id"`
	ObjectType uint8  `json:"object_type"`
	Content    string `json:"content"`
	ParentID   uint64 `json:"parent_id"`
}

type GetCommentREQ struct {
	ID         uint64 `json:"id"`
	ObjectID   uint64 `json:"object_id"`
	ObjectType uint8  `json:"object_type"`
	Page       int    `json:"page"`
	Size       int    `json:"size"`
	IsRoot     bool   `json:"is_root"`
}

type GetCommentACK struct {
	ID         uint64
	UserID     uint64
	UserName   string
	UserAvatar string
	Time       int64
	Content    string
	//Like       uint64
	Floor uint64
}
