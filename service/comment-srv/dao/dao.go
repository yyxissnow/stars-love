package dao

import (
	"gorm.io/gorm"
	"stars/conn/db"
	"stars/service/like-srv/dao"
	"time"
)

type CommentType uint8

const (
	ArticleComment CommentType = iota
	QuestionComment
	CommentComment
)

type Comment struct {
	ID          uint64      `gorm:"column:id;primary_key;AUTO_INCREMENT;not null" json:"id"`
	CreatedAt   time.Time   `gorm:"column:created_at"`
	UpdatedAt   time.Time   `gorm:"column:updated_at"`
	DeletedAt   *time.Time  `gorm:"column:deleted_at" sql:"index"`
	Type        CommentType `json:"type"`
	CommentedID uint64      `json:"commented_id"`
	ParentID    uint64      `json:"comment_id"` //为0则是root评论
	Floor       uint64      `json:"floor"`      //楼层数
	UserID      uint64      `json:"user_id"`
	Time        int64       `json:"time"`
	Content     string      `json:"content"`
	Likes       []*dao.Like `json:"likes" gorm:"foreignKey:liked_id"`
}

func (c *Comment) CreateComment() error {
	return db.GetMysqlDB().Model(Comment{}).Create(&c).Error
}

func QueryComment(id uint64, types uint32, comments *[]Comment) error {
	return db.GetMysqlDB().Order("id desc").Where("commented_id=? AND type=?", id, types).Find(&comments).Error
}

func GetCommentCount(types uint32, id uint64, num *int64) error {
	return db.GetMysqlDB().Model(Comment{}).Where("commented_id=? AND type=?", id, types).Count(num).Error
}

func AddCommentFloor(id uint64) error {
	return db.GetMysqlDB().Model(Comment{}).Where("id = ?", id).Update("floor", gorm.Expr("floor+?", 1)).Error
}

func QueryRootComment(id uint64, types uint8, size, page int, comments *[]Comment) error {
	return db.GetMysqlDB().Order("id desc").Where("commented_id=? AND type=? AND parent_id=?", id, types, 0).Limit(size).Offset((page - 1) * size).Find(&comments).Error
}

func QueryChildComment(id uint64, types uint8, size, page int, comments *[]Comment) error {
	return db.GetMysqlDB().Order("id desc").Where("commented_id=? AND type=?", id, types).Not("parent_id=?", 0).Limit(size).Offset((page - 1) * size).Find(&comments).Error
}
