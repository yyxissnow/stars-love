package main

import (
	"fmt"
	"stars/conn/config"
	"stars/conn/db"
	"stars/conn/etcd"
	"stars/log"
	account "stars/service/account-srv/account-rpc/client"
	"stars/service/article-srv/article-api/api"
	comment "stars/service/comment-srv/comment-rpc/client"
	like "stars/service/like-srv/like-rpc/client"
	"stars/util/common"
)

func main() {
	log.Init("article-api")
	cfg := config.InitConf()
	etcd.Init(cfg.EtcdConf.Address, cfg.EtcdConf.Timeout)
	etcd.RegisterEtcd("article.api", common.GetInIP()+cfg.ServerConf.ArticleApiAddr)
	db.InitDB()
	router := api.InitRouter()
	initAllCli()

	if err := router.Run(cfg.ServerConf.ArticleApiAddr); err != nil {
		log.Logger.Error(fmt.Sprintf("%s run err : %v", "article-api", err))
		return
	}
}

func initAllCli() {
	account.InitAccountCli()
	like.InitLikeCli()
	comment.InitCommentCli()
}
