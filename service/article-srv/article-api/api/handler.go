package api

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"stars/conn/oss"
	"stars/log"
	accountCli "stars/service/account-srv/account-rpc/client"
	"stars/service/article-srv/article-api/dto"
	"stars/service/article-srv/dao"
	commentCli "stars/service/comment-srv/comment-rpc/client"
	commentModel "stars/service/comment-srv/dao"
	likeModel "stars/service/like-srv/dao"
	likeCli "stars/service/like-srv/like-rpc/client"
	"stars/util/response"
	"strings"
	"time"
)

func AddArticle(c *gin.Context) {
	var req dto.AddArticleREQ
	jsonStr := c.PostForm("data")
	if jsonStr != "" {
		jsonStr = strings.Replace(jsonStr, "\n", "❀", -1) //替换换行符，不然无法解析
		err := json.Unmarshal([]byte(jsonStr), &req)
		if err != nil {
			response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
			return
		}
	} else {
		response.ResError(c, response.ErrFormIsNull, fmt.Sprintf("%d  err:jsonStr==nil", req.ID))
		return
	}
	if req.Content == "" {
		response.ResError(c, response.ErrContentIsNull, fmt.Sprintf("%d  content is null err", req.ID))
		return
	}

	req.Content = strings.Replace(req.Content, "❀", "\n", -1)
	article := dao.NewArticle()
	article.UserID = req.ID
	article.Content = req.Content
	article.Time = time.Now().Unix()
	if c.PostForm("len") != "0" {
		file, err := c.FormFile("article_Img")
		if err != nil {
			response.ResError(c, response.ErrGetAvatar, fmt.Sprintf("%d get avatar err:  %v", req.ID, err))
			return
		}

		times := strings.Replace(time.Now().Format("2006-01-02 15:04:05"), " ", "-", -1)
		times = strings.Replace(times, ":", "-", -1)
		fileName := fmt.Sprintf("articleImg/%v_%d.png", times, req.ID)

		err = oss.UploadImg(file, fileName)
		if err != nil {
			response.ResError(c, response.ErrUploadOSS, fmt.Sprintf("%d upload oss articleImg %v", req.ID, err))
			return
		}

		article.Picture = fileName
	}
	if err := article.CreateArticle(); err != nil {
		response.ResError(c, response.ErrAddArticle, fmt.Sprintf("%d add article err : %v", req.ID, err))
		return
	}
	log.Logger.Info(fmt.Sprintf("%d add article success", req.ID))
	response.ResSuccess(c)
	return
}

func GetSingleArticle(c *gin.Context) {
	var req dto.GetSingleArticleREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	var articles []dao.Article
	if err := dao.GetSingleArticle(req.UserID, &articles); err != nil {
		response.ResError(c, response.ErrQuerySingleArticle, fmt.Sprintf("%d get single articles err %v", req.ID, err))
		return
	}
	rsp, err := accountCli.GetIDUser(req.UserID)
	if err != nil {
		response.ResError(c, response.ErrGetIDUser, fmt.Sprintf("%d account-srv get user err %v", req.ID, errors.Cause(err)))
		return
	}
	var ack []dto.QueryArticleACK
	for _, value := range articles {
		if value.Picture != "" {
			value.Picture = oss.DownloadUrl(value.Picture)
		}
		ack = append(ack, dto.QueryArticleACK{
			ArticleID:  value.ID,
			UserID:     value.UserID,
			UserAvatar: oss.DownloadUrl(rsp.Avatar),
			UserName:   rsp.Name,
			Content:    value.Content,
			Time:       value.Time,
			LikeNum:    len(value.Likes),
			Picture:    value.Picture,
			CommentNum: len(value.Comments),
			LineNum:    strings.Count(value.Content, "\n") + 1,
		})
	}
	log.Logger.Info(fmt.Sprintf("%d query single articles success", req.ID))
	response.ResSuccessData(c, ack)
}

func QueryArticle(c *gin.Context) {
	var req dto.QueryArticleREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	var articles []dao.Article
	if err := dao.GetArticles(&articles); err != nil {
		response.ResError(c, response.ErrQueryArticle, fmt.Sprintf("%d get articles err %v", req.ID, err))
		return
	}

	var ack []dto.QueryArticleACK
	for _, value := range articles {
		rsp, err := accountCli.GetIDUser(value.UserID)
		if err != nil {
			response.ResError(c, response.ErrGetIDUser, fmt.Sprintf("%d account-srv get user err %v", req.ID, errors.Cause(err)))
			return
		}
		rspIsLike, err := likeCli.IsLike(req.ID, value.ID, uint32(likeModel.ArticleLike))
		//TODO:对err中没查询到进行判断
		//TODO:对两个like的rpc请求进行合并
		if err != nil {
			response.ResError(c, response.ErrIsLike, fmt.Sprintf("%d is like err %v", req.ID, errors.Cause(err)))
			return
		}

		rspLikeCount, err := likeCli.GetLikeCount(req.ID, value.ID, uint32(likeModel.ArticleLike))
		if err != nil {
			response.ResError(c, response.ErrGetLikeCount, fmt.Sprintf("%d like get like count err %v", req.ID, errors.Cause(err)))
			return
		}
		rspCommentCount, err := commentCli.GetCommentCount(req.ID, value.ID, uint32(commentModel.ArticleComment))
		if err != nil {
			response.ResError(c, response.ErrGetCommentCount, fmt.Sprintf("%d comment get comment count err %v", req.ID, errors.Cause(err)))
			return
		}
		if value.ExistPicture() {
			value.Picture = oss.DownloadUrl(value.Picture)
		}
		ack = append(ack, dto.QueryArticleACK{
			ArticleID:  value.ID,
			UserID:     value.UserID,
			Phone:      rsp.Phone,
			UserAvatar: oss.DownloadUrl(rsp.Avatar),
			UserName:   rsp.Name,
			Content:    value.Content,
			Time:       value.Time,
			Picture:    value.Picture,
			IsLike:     rspIsLike.Is,
			LikeNum:    int(rspLikeCount.Int),
			CommentNum: int(rspCommentCount.Int),
			LineNum:    strings.Count(value.Content, "\n") + 1,
		})
	}
	log.Logger.Info(fmt.Sprintf("%d query articles success", req.ID))
	response.ResSuccessData(c, ack)
}
