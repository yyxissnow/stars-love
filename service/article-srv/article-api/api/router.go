package api

import (
	"github.com/gin-gonic/gin"
)

func InitRouter() *gin.Engine {
	r := gin.New()
	v1 := r.Group("/stars")
	{
		v1.POST("/article/add", AddArticle)
		v1.POST("/article/query", QueryArticle)
		v1.POST("/article/get_single", GetSingleArticle)
	}
	return r
}
