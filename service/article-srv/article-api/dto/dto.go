package dto

type AddArticleREQ struct {
	ID      uint64 `json:"id"`
	Content string `json:"content"`
}

type GetSingleArticleREQ struct {
	ID     uint64 `json:"id"`
	UserID uint64 `json:"user_id"`
}

type GetSingleArticleACK struct {
	ArticleID  uint64 `json:"article_id"`
	UserID     uint64 `json:"user_id"`
	UserAvatar string `json:"user_avatar"`
	UserName   string `json:"user_name"`
	Content    string `json:"content"`
	Time       int64  `json:"time"`
	Picture    string `json:"picture"`
	LikeNum    int    `json:"like_num"`
	CommonNum  int    `json:"common_num"`
	LineNum    int    `json:"line_num"`
}

type QueryArticleREQ struct {
	ID uint64 `json:"id"`
}

type QueryArticleACK struct {
	ArticleID  uint64 `json:"article_id"`
	UserID     uint64 `json:"user_id"`
	Phone      string `json:"phone"`
	UserAvatar string `json:"user_avatar"`
	UserName   string `json:"user_name"`
	Content    string `json:"content"`
	Time       int64  `json:"time"`
	Picture    string `json:"picture"`
	IsLike     bool   `json:"is_like"` //是否点赞
	LikeNum    int    `json:"like_num"`
	CommentNum int    `json:"comment_num"`
	LineNum    int    `json:"line_num"`
}
