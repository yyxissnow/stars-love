package rpc

import (
	"context"
	"github.com/pkg/errors"
	article "stars/service/article-srv/article-rpc/proto"
	"stars/service/article-srv/dao"
	"stars/util/response"
)

type ArticleService struct{}

func (h *ArticleService) GetArticleContent(ctx context.Context, req *article.GetArticleContentREQ) (*article.GetArticleContentRSP, error) {
	var art dao.Article
	rsp := &article.GetArticleContentRSP{}
	if err := dao.GetArticle(req.Id, &art); err != nil {
		rsp.Code = int32(response.ErrRPCCode)
		rsp.Err = err.Error()
		return rsp, errors.Wrap(err, "get article db err")
	}
	rsp.Code = int32(response.StatusRPCOK)
	rsp.Content = art.Content
	return rsp, nil
}
