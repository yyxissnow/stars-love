package main

import (
	"fmt"
	"google.golang.org/grpc"
	"net"
	"stars/conn/config"
	"stars/conn/db"
	"stars/log"
	article "stars/service/article-srv/article-rpc/proto"
	"stars/service/article-srv/article-rpc/rpc"
)

func main() {
	log.Init("article-rpc")
	cfg := config.InitConf()
	db.InitDB()
	server := grpc.NewServer()
	article.RegisterArticleServerServer(server, new(rpc.ArticleService))
	lis, err := net.Listen("tcp", cfg.ServerConf.AccountRpcAddr)
	if err != nil {
		log.Logger.Panic(fmt.Sprintf("net listen err:%v", err))
		return
	}

	log.Logger.Info("article rpc server run")
	if err := server.Serve(lis); err != nil {
		log.Logger.Error(fmt.Sprintf("%s run err : %v", "article-rpc", err))
		return
	}
}
