package client

import (
	"context"
	"google.golang.org/grpc"
	"log"
	"stars/conn/config"
	article "stars/service/article-srv/article-rpc/proto"
)

var articleCli article.ArticleServerClient

func InitArticleCli() {
	conn, err := grpc.Dial(config.GetCfg().ServerConf.ArticleRpcAddr, grpc.WithInsecure())
	if err != nil {
		log.Panicf("article rpc init err:%v", err)
		return
	}
	articleCli = article.NewArticleServerClient(conn)
}

func GetArticleContent(id uint64) (*article.GetArticleContentRSP, error) {
	return articleCli.GetArticleContent(context.TODO(), &article.GetArticleContentREQ{
		Id: id,
	})
}
