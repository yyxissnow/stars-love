package dao

import (
	"stars/conn/db"
	commentModel "stars/service/comment-srv/dao"
	likeModel "stars/service/like-srv/dao"
	"time"
)

type Article struct {
	ID        uint64     `gorm:"column:id;primary_key;AUTO_INCREMENT;not null" json:"id"`
	CreatedAt time.Time  `gorm:"column:created_at"`
	UpdatedAt time.Time  `gorm:"column:updated_at"`
	DeletedAt *time.Time `gorm:"column:deleted_at" sql:"index"`
	UserID    uint64     `json:"user_id"`
	Content   string
	Time      int64
	Likes     []*likeModel.Like `gorm:"foreignKey:LikedID"`
	Picture   string
	Comments  []*commentModel.Comment `gorm:"foreignKey:CommentedID"`
	//Hot      int        //热度
	//TagID    uint64
}

func NewArticle() *Article {
	u := &Article{}
	u.Likes = make([]*likeModel.Like, 0)
	u.Comments = make([]*commentModel.Comment, 0)
	return u
}

func (a *Article) ExistPicture() bool {
	if a.Picture != "" {
		return true
	}
	return false
}

func (a *Article) CreateArticle() error {
	return db.GetMysqlDB().Create(&a).Error
}

func GetArticles(articles *[]Article) error {
	return db.GetMysqlDB().Order("id desc").Find(&articles).Error
}

func GetSingleArticle(id uint64, articles *[]Article) error {
	return db.GetMysqlDB().Order("id desc").Where("user_id=?", id).Find(&articles).Error
}

func GetArticle(id uint64, article *Article) error {
	return db.GetMysqlDB().Where("id = ?", id).Find(&article).Error
}
