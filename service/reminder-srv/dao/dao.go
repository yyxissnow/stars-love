package dao

import (
	"stars/conn/db"
	"time"
)

type Reminder struct {
	ID        uint64     `gorm:"column:id;primary_key;AUTO_INCREMENT;not null" json:"id"`
	CreatedAt time.Time  `gorm:"column:created_at"`
	UpdatedAt time.Time  `gorm:"column:updated_at"`
	DeletedAt *time.Time `gorm:"column:deleted_at" sql:"index"`
	UserID    uint64     `json:"user_id"`
	Title     string     `json:"title"`
	Content   string     `json:"content"`
	Time      int64      `json:"time"`
}

func (r *Reminder) AddReminder() error {
	return db.GetMysqlDB().Create(&r).Error
}

func QueryReminder(id uint64, reminders *[]Reminder) error {
	return db.GetMysqlDB().Order("id desc").Where("user_id=? ", id).Find(&reminders).Error
}

func (r *Reminder) GetReminder(id uint64) error {
	return db.GetMysqlDB().Where("id=?", id).First(&r).Error
}

func (r *Reminder) DeleteReminder() error {
	return db.GetMysqlDB().Delete(&r).Error
}

func (r *Reminder) SaveReminder() error {
	return db.GetMysqlDB().Save(&r).Error
}
