package main

import (
	"fmt"
	"stars/conn/config"
	"stars/conn/db"
	"stars/conn/etcd"
	"stars/log"
	account "stars/service/account-srv/account-rpc/client"
	"stars/service/reminder-srv/reminder-api/api"
	"stars/util/common"
)

func main() {
	log.Init("reminder-api")
	cfg := config.InitConf()
	db.InitDB()
	etcd.Init(cfg.EtcdConf.Address, cfg.EtcdConf.Timeout)
	etcd.RegisterEtcd("reminder.api", common.GetInIP()+cfg.ServerConf.ReminderApiAddr)
	router := api.InitRouter()
	initAllCli()

	if err := router.Run(cfg.ServerConf.ReminderApiAddr); err != nil {
		log.Logger.Error(fmt.Sprintf("%s run err : %v", "reminder-api", err))
		return
	}
}

func initAllCli() {
	account.InitAccountCli()
}
