package api

import (
	"github.com/gin-gonic/gin"
)

func InitRouter() *gin.Engine {
	r := gin.New()
	v1 := r.Group("/stars")
	{
		v1.POST("/reminder/add", AddReminder)
		v1.POST("/reminder/query", QueryReminder)
		v1.POST("/reminder/delete", DeleteReminder)
		v1.POST("/reminder/alter", AlterReminder)
	}
	return r
}
