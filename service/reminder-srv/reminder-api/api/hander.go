package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"stars/log"
	accountCli "stars/service/account-srv/account-rpc/client"
	accountModel "stars/service/account-srv/dao"
	"stars/service/reminder-srv/dao"
	"stars/service/reminder-srv/reminder-api/dto"
	"stars/util/response"
	"time"
)

func AddReminder(c *gin.Context) {
	var req dto.AddReminderREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	rsp, err := accountCli.GetIDUser(req.ID)
	if err != nil {
		response.ResError(c, response.ErrGetIDUser, fmt.Sprintf("%d account-srv get user err %v", req.ID, errors.Cause(err)))
		return
	}
	if accountModel.ActorType(rsp.Actor) != accountModel.Love {
		response.ResError(c, response.ErrUserNoLover, fmt.Sprintf("%d user no lover", req.ID))
		return
	}
	reminder := dao.Reminder{
		UserID:  req.ID,
		Title:   req.Title,
		Content: req.Content,
		Time:    time.Now().Unix(),
	}
	if err = reminder.AddReminder(); err != nil {
		response.ResError(c, response.ErrAddReminder, fmt.Sprintf("%d add reminder err : %v", req.ID, err))
		return
	}
	log.Logger.Info(fmt.Sprintf("%d add reminder success", req.ID))
	response.ResSuccess(c)
}

func AlterReminder(c *gin.Context) {
	var req dto.AlterReminderREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	var reminder dao.Reminder
	if err := reminder.GetReminder(req.ReminderID); err != nil {
		response.ResError(c, response.ErrGetReminder, fmt.Sprintf("%d db no find reminder %v", req.ID, err))
		return
	}
	reminder.Title = req.Title
	reminder.Content = req.Content
	if err := reminder.SaveReminder(); err != nil {
		response.ResError(c, response.ErrSaveReminder, fmt.Sprintf("%d db save reminder %v", req.ID, err))
		return
	}
	log.Logger.Info(fmt.Sprintf("%d reminder alter success", req.ID))
	response.ResSuccess(c)
}

func DeleteReminder(c *gin.Context) {
	var req dto.DeleteReminderREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	var reminder dao.Reminder
	if err := reminder.GetReminder(req.ReminderID); err != nil {
		response.ResError(c, response.ErrGetReminder, fmt.Sprintf("%d db no find reminder %v", req.ID, err))
		return
	}
	if err := reminder.DeleteReminder(); err != nil {
		response.ResError(c, response.ErrGetReminder, fmt.Sprintf("%d db delete reminder %v", req.ID, err))
		return
	}
	log.Logger.Info(fmt.Sprintf("%d reminder delete success", req.ID))
	response.ResSuccess(c)
}

func QueryReminder(c *gin.Context) {
	var req dto.QueryReminderREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	var ack []*dto.QueryReminderACK
	var reminders []dao.Reminder
	if err := dao.QueryReminder(req.ID, &reminders); err != nil {
		response.ResError(c, response.ErrQueryReminder, fmt.Sprintf("%d db query reminder %v", req.ID, err))
		return
	}
	for _, value := range reminders {
		ack = append(ack, &dto.QueryReminderACK{
			ID:      value.ID,
			Title:   value.Title,
			Content: value.Content,
			Time:    value.Time,
		})
	}
	log.Logger.Info(fmt.Sprintf("%d query reminder success", req.ID))
	response.ResSuccessData(c, &ack)
}
