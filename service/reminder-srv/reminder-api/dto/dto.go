package dto

type AddReminderREQ struct {
	ID      uint64 `json:"id"`
	Title   string `json:"title"`
	Content string `json:"content"`
}

type AlterReminderREQ struct {
	ID         uint64 `json:"id"`
	ReminderID uint64 `json:"wish_id"`
	Title      string `json:"title"`
	Content    string `json:"content"`
}

type DeleteReminderREQ struct {
	ID         uint64 `json:"id"`
	ReminderID uint64 `json:"wish_id"`
}

type QueryReminderREQ struct {
	ID uint64 `json:"id"`
}

type QueryReminderACK struct {
	ID      uint64 `json:"id"`
	Title   string `json:"title"`
	Content string `json:"content"`
	Time    int64  `json:"time"`
}
