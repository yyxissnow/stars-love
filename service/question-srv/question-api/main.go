package main

import (
	"fmt"
	"stars/conn/config"
	"stars/conn/db"
	"stars/conn/etcd"
	"stars/log"
	account "stars/service/account-srv/account-rpc/client"
	comment "stars/service/comment-srv/comment-rpc/client"
	like "stars/service/like-srv/like-rpc/client"
	"stars/service/question-srv/question-api/api"
	"stars/util/common"
)

func main() {
	log.Init("question-api")
	cfg := config.InitConf()
	db.InitDB()
	etcd.Init(cfg.EtcdConf.Address, cfg.EtcdConf.Timeout)
	etcd.RegisterEtcd("question.api", common.GetInIP()+cfg.ServerConf.QuestionAPIAddr)
	router := api.InitRouter()
	initAllCli()

	if err := router.Run(cfg.ServerConf.QuestionAPIAddr); err != nil {
		log.Logger.Error(fmt.Sprintf("%s run err : %v", "question-api", err))
		return
	}
}

func initAllCli() {
	account.InitAccountCli()
	like.InitLikeCli()
	comment.InitCommentCli()
}
