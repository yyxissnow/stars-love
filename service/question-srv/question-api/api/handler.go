package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"stars/conn/oss"
	"stars/log"
	accountCli "stars/service/account-srv/account-rpc/client"
	commentCli "stars/service/comment-srv/comment-rpc/client"
	commentModel "stars/service/comment-srv/dao"
	likeModel "stars/service/like-srv/dao"
	likeCli "stars/service/like-srv/like-rpc/client"
	"stars/service/question-srv/dao"
	"stars/service/question-srv/question-api/dto"
	"stars/util/response"
	"time"
)

func AddQuestion(c *gin.Context) {
	var req dto.AddQuestionREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	question := dao.NewQuestion()
	question.Title = req.Title
	question.Time = time.Now().Unix()

	if err := question.AddQuestion(); err != nil {
		response.ResError(c, response.ErrAddQuestion, fmt.Sprintf("%d db add question err : %v", req.ID, err))
		return
	}
	log.Logger.Info(fmt.Sprintf("%d  add question success", req.ID))
	response.ResSuccess(c)
}

func GetSingleQuestion(c *gin.Context) {
	var req dto.GetMyQuestionREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	var questions []dao.Question
	if err := dao.GetSingleQuestion(req.UserID, &questions); err != nil {
		response.ResError(c, response.ErrQuerySingleQuestion, fmt.Sprintf("%d get single questions err %v", req.ID, err))
		return
	}
	rsp, err := accountCli.GetIDUser(req.UserID)
	if err != nil {
		response.ResError(c, response.ErrGetIDUser, fmt.Sprintf("%d account-srv get user err %v", req.ID, errors.Cause(err)))
		return
	}
	var ack []dto.QueryQuestionACK
	for _, value := range questions {
		ack = append(ack, dto.QueryQuestionACK{
			QuestionID: value.ID,
			UserID:     value.UserID,
			UserAvatar: oss.DownloadUrl(rsp.Avatar),
			UserName:   rsp.Name,
			Title:      value.Title,
			Time:       value.Time,
			LikeNum:    len(value.Likes),
			CommonNum:  len(value.Comments),
		})
	}
	log.Logger.Info(fmt.Sprintf("%d query single questions success", req.ID))
	response.ResSuccessData(c, ack)
}

func QueryQuestion(c *gin.Context) {
	var req dto.QueryQuestionREQ
	if err := c.ShouldBindJSON(&req); err != nil {
		response.ResError(c, response.ErrBindJson, fmt.Sprintf("%d  err:bind json err %v", req.ID, err))
		return
	}
	var questions []dao.Question
	if err := dao.GetQuestion(&questions); err != nil {
		response.ResError(c, response.ErrQueryQuestion, fmt.Sprintf("%d get questions err %v", req.ID, err))
		return
	}

	var ack []dto.QueryQuestionACK
	for _, value := range questions {
		rsp, err := accountCli.GetIDUser(value.UserID)
		if err != nil {
			response.ResError(c, response.ErrGetIDUser, fmt.Sprintf("%d account-srv get user err %v", req.ID, errors.Cause(err)))
			return
		}
		rspIsLike, err := likeCli.IsLike(req.ID, value.ID, uint32(likeModel.QuestionLike))
		//TODO:对err中没查询到进行判断
		//TODO:对两个like的rpc请求进行合并
		if err != nil {
			response.ResError(c, response.ErrIsLike, fmt.Sprintf("%d is like err %v", req.ID, errors.Cause(err)))
			return
		}

		rspLikeCount, err := likeCli.GetLikeCount(req.ID, value.ID, uint32(likeModel.QuestionLike))
		if err != nil {
			response.ResError(c, response.ErrGetLikeCount, fmt.Sprintf("%d like get like count err %v", req.ID, errors.Cause(err)))
			return
		}
		rspCommentCount, err := commentCli.GetCommentCount(req.ID, value.ID, uint32(commentModel.QuestionComment))
		if err != nil {
			response.ResError(c, response.ErrGetCommentCount, fmt.Sprintf("%d comment get comment count err %v", req.ID, errors.Cause(err)))
			return
		}
		ack = append(ack, dto.QueryQuestionACK{
			QuestionID: value.ID,
			UserID:     value.UserID,
			Phone:      rsp.Phone,
			UserAvatar: oss.DownloadUrl(rsp.Avatar),
			UserName:   rsp.Name,
			Title:      value.Title,
			Time:       value.Time,
			IsLike:     rspIsLike.Is,
			LikeNum:    int(rspLikeCount.Int),
			CommonNum:  int(rspCommentCount.Int),
		})
	}
	log.Logger.Info(fmt.Sprintf("%d query questions success", req.ID))
	response.ResSuccessData(c, ack)
}
