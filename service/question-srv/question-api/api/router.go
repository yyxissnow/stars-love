package api

import (
	"github.com/gin-gonic/gin"
)

func InitRouter() *gin.Engine {
	r := gin.New()
	v1 := r.Group("/stars")
	{
		v1.POST("/question/add", AddQuestion)
		v1.POST("/question/query", QueryQuestion)
		v1.POST("/question/get_single_question", GetSingleQuestion)
	}
	return r
}
