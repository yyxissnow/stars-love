package dto

type AddQuestionREQ struct {
	ID    uint64 `json:"id"`
	Title string `json:"title"`
}

type GetMyQuestionREQ struct {
	ID     uint64 `json:"id"`
	UserID uint64 `json:"user_id"`
}

type GetMyQuestionACK struct {
	QuestionID uint64 `json:"question_id"`
	UserID     uint64 `json:"user_id"`
	UserAvatar string `json:"user_avatar"`
	UserName   string `json:"user_name"`
	Title      string `json:"title"`
	Time       int64  `json:"time"`
	LikeNum    int    `json:"like_num"`
	CommonNum  int    `json:"common_num"`
}

type QueryQuestionREQ struct {
	ID uint64 `json:"id"`
}

type QueryQuestionACK struct {
	QuestionID uint64 `json:"question_id"`
	UserID     uint64 `json:"user_id"`
	Phone      string `json:"phone"`
	UserAvatar string `json:"user_avatar"`
	UserName   string `json:"user_name"`
	Title      string `json:"title"`
	Time       int64  `json:"time"`
	IsLike     bool   `json:"is_like"`
	LikeNum    int    `json:"like_num"`
	CommonNum  int    `json:"common_num"`
}

type CommentQuestionREQ struct {
	ID         uint64 `json:"id"`
	QuestionID uint64 `json:"question_id"`
	Comment    string `json:"comment"`
	CommentID  uint64 `json:"comment_id"`
}
