package dao

import (
	"stars/conn/db"
	commentModel "stars/service/comment-srv/dao"
	likeModel "stars/service/like-srv/dao"
	"time"
)

type Question struct {
	ID        uint64     `gorm:"column:id;primary_key;AUTO_INCREMENT;not null" json:"id"`
	CreatedAt time.Time  `gorm:"column:created_at"`
	UpdatedAt time.Time  `gorm:"column:updated_at"`
	DeletedAt *time.Time `gorm:"column:deleted_at" sql:"index"`
	UserID    uint64     `json:"user_id"`
	Title     string
	Time      int64
	Likes     []likeModel.Like       `gorm:"foreignKey:liked_id"`
	Comments  []commentModel.Comment `gorm:"foreignKey:commented_id"`
	//Hot      int //热度
}

func NewQuestion() *Question {
	u := &Question{}
	u.Likes = make([]likeModel.Like, 0)
	u.Comments = make([]commentModel.Comment, 0)
	return u
}

func (q *Question) AddQuestion() error {
	return db.GetMysqlDB().Create(&q).Error
}

func GetQuestion(question *[]Question) error {
	return db.GetMysqlDB().Order("id desc").Find(&question).Error
}

func GetSingleQuestion(id uint64, questions *[]Question) error {
	return db.GetMysqlDB().Order("id desc").Where("user_id=?", id).Find(&questions).Error
}
