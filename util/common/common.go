package common

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"net"
	"net/http"
	"stars/conn/config"
	"strings"
	"time"
)

//指定位数随机字符串
func RandString(lenNum int) string {
	var chars = []string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"}
	str := ""
	length := len(chars)
	rand.Seed(time.Now().UnixNano()) //重新播种，否则值不会变
	for i := 0; i < lenNum; i++ {
		str += chars[rand.Intn(length)] // +号拼接字符性能低，需要copy,频繁新增释放操作
	}
	return str
}

//获取本地对外ip
func GetInIP() string {
	if config.GetCfg().Basic.Run == "debug" {
		return "127.0.0.1"
	}
	conn, err := net.Dial("udp", "8.8.8.8:88")
	if err != nil {
		fmt.Println("get ip err,", err)
		return ""
	}
	defer conn.Close()
	localAddr := conn.LocalAddr().(*net.UDPAddr)
	return strings.Split(localAddr.IP.String(), ":")[0]
}

// 获取外网 IP
func GetOutIP() string {
	responseClient, errClient := http.Get("http://ip.dhcp.cn/?ip")
	if errClient != nil {
		fmt.Printf("获取外网 IP 失败，请检查网络\n")
		panic(errClient)
	}
	// 程序在使用完 response 后必须关闭 response 的主体。
	defer responseClient.Body.Close()

	body, _ := ioutil.ReadAll(responseClient.Body)
	clientIP := fmt.Sprintf("%s", string(body))
	return clientIP
}
