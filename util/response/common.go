package response

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"stars/log"
)

const (
	SuccessCode = 20000
)

type Success struct {
	Code int         `json:"code"`
	Data interface{} `json:"data,omitempty"`
}

type Error struct {
	Code    ErrorCode `json:"code"`
	Message string    `json:"msg"`
}

type PageSearch struct {
	Keyword  string
	Page     int
	PageSize int
}

func ResSuccess(c *gin.Context) {
	c.JSON(http.StatusOK, Success{Code: SuccessCode})
}

func ResSuccessData(c *gin.Context, data interface{}) {
	c.JSON(http.StatusOK, Success{Code: SuccessCode, Data: data})
}

func ResError(c *gin.Context, code ErrorCode, msg string) {
	log.Logger.Error(msg)
	c.JSON(http.StatusOK, Error{Code: code, Message: msg})
}
