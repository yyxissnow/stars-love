package response

type ErrorCode int

const (
	StatusRPCOK ErrorCode = 900
	ErrRPCCode  ErrorCode = 901

	ErrService        ErrorCode = 1000
	ErrNotToken       ErrorCode = 1001
	ErrParseToken     ErrorCode = 1002
	ErrAgentInfoValid ErrorCode = 1003
	ErrHostAddr       ErrorCode = 1004
	ErrBindJson       ErrorCode = 1005
	ErrFormIsNull     ErrorCode = 1006
	ErrUploadOSS      ErrorCode = 1007
	ErrStringToInt    ErrorCode = 1008

	ErrLoginInfo        ErrorCode = 2001
	ErrGenToken         ErrorCode = 2002
	ErrRegisterInfoNull ErrorCode = 2003
	ErrPhoneNum         ErrorCode = 2004
	ErrUserExited       ErrorCode = 2005
	ErrGetAvatar        ErrorCode = 2006
	ErrSaveAvatar       ErrorCode = 2007
	ErrCreateUser       ErrorCode = 2008
	ErrGetIDUser        ErrorCode = 2009

	ErrGetLoverInfo ErrorCode = 3001
	ErrUserNoSingle ErrorCode = 3002
	ErrCreateLover  ErrorCode = 3003
	ErrUserNoLover  ErrorCode = 3004

	ErrSendMsg            ErrorCode = 4001
	ErrFindMsg            ErrorCode = 4002
	ErrAddWishList        ErrorCode = 5001
	ErrQueryNoWish        ErrorCode = 5002
	ErrQueryYesWish       ErrorCode = 5003
	ErrGetWish            ErrorCode = 5004
	ErrSaveWish           ErrorCode = 5005
	ErrDeleteWish         ErrorCode = 5006
	ErrAddReminder        ErrorCode = 6001
	ErrQueryReminder      ErrorCode = 6002
	ErrGetReminder        ErrorCode = 6003
	ErrSaveReminder       ErrorCode = 6004
	ErrAddPrivateChat     ErrorCode = 6005
	ErrQueryPrivateChat   ErrorCode = 6006
	ErrUpdateWebSocket    ErrorCode = 6007
	ErrChatSendMsg        ErrorCode = 6008
	ErrChatRecvMsg        ErrorCode = 6009
	ErrAddDiaryMarkDay    ErrorCode = 7001
	ErrAddReminderMarkDay ErrorCode = 7002
	ErrQueryDiaryDay      ErrorCode = 7003
	ErrQueryReminderDay   ErrorCode = 7004

	ErrContentIsNull       ErrorCode = 8001
	ErrAddArticle          ErrorCode = 8002
	ErrQueryArticle        ErrorCode = 8003
	ErrQuerySingleArticle  ErrorCode = 8004
	ErrLikeArticle         ErrorCode = 8005
	ErrGetArticleContent   ErrorCode = 8006
	ErrGetLikeCount        ErrorCode = 9001
	ErrIsLike              ErrorCode = 9002
	ErrGetLimitLike        ErrorCode = 9003
	ErrDeleteLike          ErrorCode = 9004
	ErrCreateLike          ErrorCode = 9005
	ErrGetCommentCount     ErrorCode = 10001
	ErrQueryComment        ErrorCode = 10002
	ErrCommentArticle      ErrorCode = 10003
	ErrAddQuestion         ErrorCode = 11001
	ErrLikeQuestion        ErrorCode = 11002
	ErrQueryQuestion       ErrorCode = 11003
	ErrQuerySingleQuestion ErrorCode = 11004
	ErrCommentQuestion     ErrorCode = 11005
)
