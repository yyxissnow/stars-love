package middleware

import (
	"github.com/gin-gonic/gin"
)

func NoMethodHandler() gin.HandlerFunc {
	return func(context *gin.Context) {
		context.JSON(405, gin.H{"message": "方法不被允许"})
	}
}

func NoRouterHandler() gin.HandlerFunc {
	return func(context *gin.Context) {
		context.JSON(500, gin.H{"message": "未找到请求路由处理函数"})
	}
}
