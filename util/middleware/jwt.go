package middleware

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"main.go/util/response"
)

type UserToken struct {
	UserID uint64
	jwt.StandardClaims
}

var MySecret = []byte("this is XCY")

func JWTMiddleware() func(c *gin.Context) {
	return func(c *gin.Context) {
		chick := TokenPermissionChick(c)
		if chick {
			c.Set("id", uint64(0))
			c.Next()
			return
		}
		authHeader := c.Request.Header.Get("token")
		if authHeader == "" {
			response.ResErrCode(c, response.ErrNotToken)
			c.Abort()
			return
		}
		mc, err := ParseToken(authHeader)
		if err != nil {
			response.ResErrCode(c, response.ErrParseToken)
			c.Abort()
			return
		}
		c.Set("id", mc.UserID)
		c.Next()
	}
}

func ParseToken(tokenSting string) (*UserToken, error) {
	token, err := jwt.ParseWithClaims(tokenSting, &UserToken{}, func(token *jwt.Token) (interface{}, error) {
		return MySecret, nil
	})
	if err != nil {
		return nil, err
	}
	if claims, ok := token.Claims.(*UserToken); ok && token.Valid {
		return claims, nil
	}
	return nil, errors.New("invalid token")
}
