package corn

import (
	"fmt"
	"github.com/robfig/cron/v3"
	"log"
	"stars/conn/db"
	"stars/service/lover-srv/dao"
)

func Init() {
	fmt.Println("join")
	c := cron.New()
	_, err := c.AddFunc("0 0 */1 * *", addLoverDay)
	//_, err = c.AddFunc("0 0 */1 * *", addLoverDay)
	//_,err:=c.AddFunc("*/1 * * * *",addLoverDay)
	if err != nil {
		log.Println("err:", err)
	}
	c.Start()
	fmt.Println("start")
	select {}
}

func addLoverDay() {
	var lover []dao.Lover
	db.GetMysqlDB().Model(dao.Lover{}).Find(&lover)
	for _, value := range lover {
		value.LoverDay++
		db.GetMysqlDB().Save(&value)
		log.Println("add day lover success id:", value.ID)
	}
}
