package main

import (
	"context"
	"fmt"
	"go.etcd.io/etcd/client/v3"
	"time"
)

func main() {
	cli, err := clientv3.New(clientv3.Config{
		Endpoints:   []string{"127.0.0.1:2379"},
		DialTimeout: 5 * time.Second, //超时时间
	})
	if err != nil {
		// handle error!
		fmt.Printf("connect to etcd failed, err:%v\n", err)
		return
	}
	fmt.Println("connect to etcd success")
	defer cli.Close()
	// put 设置值
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*1) //做一个超时控制
	value := `[{"path":"f:/go/stars-love/bin/log/register_client/output.log","topic":"stars_log"},
			 {"path":"f:/go/stars-love/bin/log/information/output.log","topic":"stars_log"},
			 {"path":"f:/go/stars-love/bin/log/message/output.log","topic":"stars_log"},
			 {"path":"f:/go/stars-love/bin/log/wishList/output.log","topic":"stars_log"}]`
	_, err = cli.Put(ctx, "/logagent/stars/collect_config", value)
	cancel()
	if err != nil {
		fmt.Printf("put to etcd failed, err:%v\n", err)
		return
	}
}
