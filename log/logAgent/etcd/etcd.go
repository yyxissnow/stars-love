package etcd

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
	"go.etcd.io/etcd/client/v3"
	"main.go/log/logAgent/common"
	"time"
)

var (
	client   *clientv3.Client
	log      *logrus.Logger
	confChan chan []*common.CollectEntry
)

//需要收集日志的配置信息
type LogEntry struct {
	Path  string `json:"path"`  //日志存放的路径
	Topic string `json:"topic"` //日志要发往kafka中的哪个Topic
}

//初始化ETCD的函数
func Init(addr string, timeout time.Duration) (err error) {
	client, err = clientv3.New(clientv3.Config{ //不能用：申明 因为申明等于初始化一个局部的cli 而全局cli永远是new
		Endpoints:   []string{addr},
		DialTimeout: timeout, //超时时间
	})
	if err != nil {
		// handle error!
		fmt.Printf("connect to etcd failed, err:%v\n", err)
		return
	}
	confChan = make(chan []*common.CollectEntry)
	return
}

//从ETCD中根据key获取配置项
func GetConf(key string) (value []*LogEntry, err error) {
	// get 获取值
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	resp, err := client.Get(ctx, key) //返回的是一个响应 里面有多个键值对
	cancel()
	if err != nil {
		fmt.Printf("get from etcd failed, err:%v\n", err)
		return
	}
	for _, ev := range resp.Kvs { //获取这些键值对
		fmt.Printf("%s:%s\n", ev.Key, ev.Value)
		err = json.Unmarshal(ev.Value, &value)
		if err != nil {
			fmt.Printf("unmarshal etcd failed, err:%v\n", err)
			return
		}
	}
	return
}

func WatchConf(key string, newConfCh chan<- []*LogEntry) {
	cli, err := clientv3.New(clientv3.Config{
		Endpoints:   []string{"127.0.0.1:2379"},
		DialTimeout: 5 * time.Second, //超时时间
	})
	if err != nil {
		// handle error!
		fmt.Printf("connect to etcd failed, err:%v\n", err)
		return
	}
	fmt.Println("connect to etcd success")
	defer cli.Close()
	ch := cli.Watch(context.Background(), key)
	//从通道尝试取值(监视的信息 只要有变化就会往通道发信息)
	for wresp := range ch {
		for _, evt := range wresp.Events { //拿到发生变化的事件
			fmt.Printf("Type: %s Key:%s Value:%s\n", evt.Type, string(evt.Kv.Key), string(evt.Kv.Value))
			//通知tailLogMgr
			//先判断操作类型
			var newConf []*LogEntry
			if evt.Type != clientv3.EventTypeDelete {
				//如果是删除操作,手动传递一个空的配置项
				err := json.Unmarshal(evt.Kv.Value, &newConf)
				if err != nil {
					fmt.Println("unmarshal failed err ", err)
					continue
				}
			}
			fmt.Println("get new conf ", newConf)
			newConfCh <- newConf
		}
	}
}

// 向外暴露一个chan
func WatchChan() <-chan []*common.CollectEntry {
	return confChan
}
