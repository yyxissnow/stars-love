package main

import (
	"fmt"
	"gopkg.in/ini.v1"
	"main.go/log/logAgent/conf"
	"main.go/log/logAgent/etcd"
	"main.go/log/logAgent/kafka"
	"main.go/log/logAgent/tailLog"
	"sync"
	"time"
)

//logAgent入口程序

var (
	cfg = new(conf.AppConf)
)

func main() {
	//加载配置文件
	//cfg,err:=ini.Load("./conf/config.ini") //简单的使用
	//关于结构体与分区双向映射
	err := ini.MapTo(cfg, "./conf/config.ini") //cfg 一定是传指针才行 所以上面定义时便初始化了
	if err != nil {
		fmt.Println("load ini failed err ", err)
		return
	}
	//初始化kafka
	err = kafka.Init([]string{cfg.KafkaConf.Address}, cfg.KafkaConf.ChanMaxSize)
	if err != nil {
		fmt.Println("setup kafka failed err ", err)
		return
	}
	fmt.Println("setup kafka success")

	//初始化ETCD
	err = etcd.Init(cfg.EtcdConf.Address, time.Duration(cfg.EtcdConf.Timeout)*time.Second) //获取的数字变量不能直接乘second 要用Duration强转一下
	if err != nil {
		fmt.Println("setup etcd failed err ", err)
		return
	}
	fmt.Println("setup etcd success")
	//为了实现每个logAgent都拉取自己独有的配置，所以要自己的IP地址作为区分
	//ipStr,err:=utils.GetOutboundIP()
	//if err!=nil{
	//	panic(err)
	//}
	//etcdConfKey:=fmt.Sprintf(cfg.EtcdConf.Key,ipStr)
	etcdConfKey := cfg.EtcdConf.Key
	//fmt.Println(etcdConfKey)
	//从etcd中获取日志收集项的信息
	logEntryConf, err := etcd.GetConf(etcdConfKey)

	if err != nil {
		fmt.Println("get conf failed err ", err)
		return
	}
	fmt.Println(logEntryConf)
	fmt.Println("get conf from etcd success ", logEntryConf)
	//派一个哨兵去监视日志收集项的变化(有变化及时通知我的logAgent 实现热加载配置)
	for index, value := range logEntryConf {
		fmt.Printf("index:%v  value:%v\n", index, value)
	}
	//收集日志发往kafka
	//循环每一个日志收集项，创建TailObj
	tailLog.Init(logEntryConf)
	//因为NewConfChan访问了tskMgr的newConfChan，这个channel是在tailLog.Init执行的初始化
	newConfChan := tailLog.NewConfChan() //从tailLog包中获取对外暴露的通道
	var wg sync.WaitGroup
	wg.Add(1)
	go etcd.WatchConf(etcdConfKey, newConfChan) //哨兵发现最新的配置信息会通知上面的那个通道
	wg.Wait()
}
