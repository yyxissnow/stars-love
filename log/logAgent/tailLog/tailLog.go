package tailLog

import (
	"context"
	"fmt"
	"github.com/hpcloud/tail"
	"main.go/log/logAgent/kafka"
)

//var (
//	tailObj *tail.Tail
//	//LogChan
//)

//一个日志收集的任务
type TailTask struct {
	path     string
	topic    string
	instance *tail.Tail //tailObj类型 具体对象
	//为了能够实现退出t.run()
	ctx        context.Context
	cancelFunc context.CancelFunc
}

func NewTailTask(path, topic string) (tailObj *TailTask) {
	ctx, cancel := context.WithCancel(context.Background())
	tailObj = &TailTask{
		path:       path,
		topic:      topic,
		ctx:        ctx,
		cancelFunc: cancel,
	}
	tailObj.init()
	return
}

func (t TailTask) init() {
	config := tail.Config{
		ReOpen:    true, //重新打开
		Poll:      true,
		Location:  &tail.SeekInfo{Offset: 0, Whence: 2}, //从文件的哪个地方开始读
		Follow:    true,                                 //是否跟随
		MustExist: false,                                //文件不存在时不报错
	}
	var err error
	t.instance, err = tail.TailFile(t.path, config)
	if err != nil {
		fmt.Println("tail file failed err ", err)
	}
	//当goroutine执行的函数退出的时候，goroutine就结束了
	go t.run() //直接去采集日志发送到kafka
}

//专门从日志文件收集日志的模块
//func Init(fileName string) (err error) {
//	config := tail.Config{
//		ReOpen:    true, //重新打开
//		Poll:      true,
//		Location:  &tail.SeekInfo{Offset: 0, Whence: 2}, //从文件的哪个地方开始读
//		Follow:    true,                                 //是否跟随
//		MustExist: false,                                //文件不存在时不报错
//	}
//	tailObj, err = tail.TailFile(fileName, config)
//	if err != nil {
//		fmt.Println("tail file failed err ", err)
//		return
//	}
//	return
//}

//func ReadChan() <-chan *tail.Line {
//	return tailObj.Lines
//}

func (t *TailTask) run() {
	for {
		select {
		case <-t.ctx.Done():
			fmt.Printf("tail task:%s 结束了...\n", t.path+" "+t.topic)
			return
		case line := <-t.instance.Lines: //从tailObj的通道中一行一行的读取日志数据
			if line.Text == "" {
				break
			}
			//发往kafka
			//kafka.SendToKafka(t.topic,line.Text)//函数调函数 即会将该函数执行完后才会进行下一个for循环 当数据量大时会影响性能
			//先把日志数据发送到一个通道中
			fmt.Printf("get log data from %s success, log:%v\n", t.path, line.Text)
			kafka.SendToChan(t.topic, line.Text)
			//kafka那个包中有单独的goroutine去取日志数据发到kafka
		}
	}
}
