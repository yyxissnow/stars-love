package tailLog

import (
	"fmt"
	"main.go/log/logAgent/etcd"
	"time"
)

var tskMgr *tailLogMgr

//tailTask管理者
type tailLogMgr struct {
	logEntry    []*etcd.LogEntry
	tskMap      map[string]*TailTask
	newConfChan chan []*etcd.LogEntry
}

func Init(logEntryConf []*etcd.LogEntry) {
	tskMgr = &tailLogMgr{
		logEntry:    logEntryConf, //把当前的日志收集项配置信息保存起来
		tskMap:      make(map[string]*TailTask, 16),
		newConfChan: make(chan []*etcd.LogEntry), //无缓冲区的通道
	}
	for _, logEntry := range logEntryConf {
		//logEntry.Path 要收集的日志文件的路径
		//初始化的时候起了多少个tailtask都要记下来，为后续判断方便
		tailObj := NewTailTask(logEntry.Path, logEntry.Topic)
		mk := fmt.Sprintf("%s_%s", logEntry.Path, logEntry.Topic)
		tskMgr.tskMap[mk] = tailObj
	}
	go tskMgr.run()
}

//监听自己的newConfChan，有了新的配置过来之后就做对应处理
func (t *tailLogMgr) run() {
	for {
		select {
		case newConf := <-t.newConfChan:
			for _, conf := range newConf {
				mk := fmt.Sprintf("%s_%s", conf.Path, conf.Topic)
				_, ok := t.tskMap[mk]
				if ok {
					//原来就有，不需要操作
					continue
				} else {
					//新增的
					tailObj := NewTailTask(conf.Path, conf.Topic)
					t.tskMap[mk] = tailObj
				}
			}
			//找出原来t.logEntry有，但是newConf中没有的，要删掉
			//因为更新时不是新加配置项，而是替换掉原来的所有配置项
			for _, c1 := range t.logEntry { //原来配置项中依次拿出配置项
				isDelete := true
				for _, c2 := range newConf { //去新的配置项中逐一比较
					if c2.Path == c1.Path && c2.Topic == c1.Topic {
						isDelete = false
						continue
					}
					if isDelete {
						//把c1对应的tailObj给停掉
						mk := fmt.Sprintf("%s_%s", c1.Path, c1.Topic)
						t.tskMap[mk].cancelFunc()
					}
				}
			}
			//配置删除
			//配置变更
			fmt.Println("新的配置来了!", newConf)
		default:
			time.Sleep(time.Second)
		}
	}
}

//一个函数，向外暴露tskMgr的newConfChan
func NewConfChan() chan<- []*etcd.LogEntry {
	return tskMgr.newConfChan
}
