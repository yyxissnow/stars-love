package kafka

import (
	"fmt"
	"github.com/Shopify/sarama"
	"time"
)

//专门往kafka写日志的模块

type LogData struct {
	topic string
	data  string
}

var (
	client      sarama.SyncProducer //申明一个全局的连接kafka的生产者client
	logDataChan chan *LogData
)

//Init 初始化client
func Init(addr []string, maxSize int) (err error) {
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll          // 发送完数据需要leader和follow都确认
	config.Producer.Partitioner = sarama.NewRandomPartitioner // 新选出一个partition
	config.Producer.Return.Successes = true                   // 成功交付的消息将在success channel返回

	// 连接kafka
	client, err = sarama.NewSyncProducer(addr, config)
	if err != nil {
		fmt.Println("producer closed, err:", err)
		return
	}
	//初始化logDataChan
	logDataChan = make(chan *LogData, maxSize)
	//开启后台的goroutine从通道中取出数据发往kafka
	go sendToKafka()
	return
}

//真正往kafka发送日志的函数
func sendToKafka() {
	for {
		select {
		case ld := <-logDataChan:
			//构造一个信息
			msg := &sarama.ProducerMessage{}
			msg.Topic = ld.topic
			msg.Value = sarama.StringEncoder(ld.data)
			//发送到kafka
			pid, offset, err := client.SendMessage(msg)
			if err != nil {
				fmt.Println("send msg failed, err:", err)
				return
			}
			fmt.Printf("pid:%v offset:%v\n", pid, offset)
		default:
			time.Sleep(time.Millisecond * 50)
		}
	}
}

//给外部暴露一个函数，该函数只把日志数据发送到一个内部的channel中
//此函数每次只需要将消息发在通道中即返回，则效率会非常快 将真正从通道中发送数据的另起函数
func SendToChan(topic, data string) {
	msg := &LogData{
		topic: topic,
		data:  data,
	}
	logDataChan <- msg
}
