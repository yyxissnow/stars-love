package es

import (
	"context"
	"fmt"
	"github.com/olivere/elastic/v7"
	"strings"
	"time"
)

//初始化ES,准备接受kafka那边发来的数据

var (
	client *elastic.Client
	ch     chan *LogData
)

type LogData struct {
	Topic    string    `json:"topic"`
	Phone    uint64    `json:"phone"`
	Location string    `json:"location"`
	Status   string    `json:"status"`
	Time     time.Time `json:"time"`
	ToDo     string    `json:"to_do"`
	//Data  string    `json:"data"`
}

func Init(addr string, chanSize, nums int) (err error) {
	if !strings.HasPrefix(addr, "http://") {
		addr = "http://" + addr
	}
	client, err = elastic.NewClient(elastic.SetURL("http://127.0.0.1:9200"))
	if err != nil {
		// Handle error
		return err
	}
	fmt.Println("connect to es success")
	ch = make(chan *LogData, chanSize)
	for i := 0; i <= nums; i++ {
		go sendToES()
	}
	return
}

//发送数据到ES
func SendToESChan(msg *LogData) {
	ch <- msg
}

func sendToES() {
	//链式操作
	for {
		select {
		case msg := <-ch:
			put1, err := client.Index().
				Index(msg.Topic).
				BodyJson(msg).
				Do(context.Background())
			if err != nil {
				fmt.Println(err)
				continue
			}
			fmt.Printf("Indexed student %s to index %s, type %s\n", put1.Id, put1.Index, put1.Type)
		default:
			time.Sleep(time.Second)
		}
	}
}
