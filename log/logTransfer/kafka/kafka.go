package kafka

import (
	"fmt"
	"github.com/Shopify/sarama"
	"main.go/log/logTransfer/es"
	"strconv"
	"strings"
	"time"
)

//初始化kafka消费者 从kafka取数据发往ES

//Init
func Init(addr []string, topic string) error {
	consumer, err := sarama.NewConsumer(addr, nil)
	if err != nil {
		fmt.Printf("fail to start consumer, err:%v\n", err)
		return err
	}
	partitionList, err := consumer.Partitions(topic) // 根据topic取到所有的分区
	if err != nil {
		fmt.Printf("fail to get list of partition:err%v\n", err)
		return err
	}
	fmt.Println(partitionList)
	for partition := range partitionList { // 遍历所有的分区
		// 针对每个分区创建一个对应的分区消费者
		pc, err := consumer.ConsumePartition(topic, int32(partition), sarama.OffsetNewest)
		if err != nil {
			fmt.Printf("failed to start consumer for partition %d,err:%v\n", partition, err)
			return err
		}
		//defer pc.AsyncClose()
		// 异步从每个分区消费信息
		go func(sarama.PartitionConsumer) {
			for msg := range pc.Messages() {
				fmt.Printf("Partition:%d Offset:%d Key:%v Value:%v\n", msg.Partition, msg.Offset, msg.Key, string(msg.Value))
				//直接发给ES
				countMsg := strings.Split(string(msg.Value), "\t")
				userMsg := strings.Split(countMsg[3], "  ")
				idInt, _ := strconv.Atoi(userMsg[0])
				idUint := uint64(idInt)
				ld := es.LogData{Topic: topic, Time: time.Now(), Status: countMsg[1], Location: countMsg[2], Phone: idUint, ToDo: userMsg[1]}
				//es.SendToES(topic,ld)//函数调函数 缺点为要该函数执行完才会进行下一次for循环
				//优化一下:直接放到一个chan中 当进入SendToES中时就直接进入下一次for循环
				es.SendToESChan(&ld)
			}
		}(pc)
	}
	return err
}
