package main

import (
	"fmt"
	"gopkg.in/ini.v1"
	"main.go/log/logTransfer/conf"
	"main.go/log/logTransfer/es"
	"main.go/log/logTransfer/kafka"
)

//将日志数据从kafka取出来发往ES

func main() {
	//加载配置文件
	var cfg = new(conf.LogTransferCfg)
	err := ini.MapTo(cfg, "./conf/cfg.ini")
	if err != nil {
		fmt.Println("setup config failed err ", err)
		return
	}
	fmt.Println("cfg:", cfg)
	//初始化ES
	//对外提供一个往ES写入数据的函数
	err = es.Init(cfg.ESCfg.Address, cfg.ESCfg.ChanSize, cfg.ESCfg.Nums)
	if err != nil {
		fmt.Println("setup ES register_client failed err ", err)
		return
	}
	//初始化kafka消费者
	//连接kafka，创建分区的消费者
	//每个分区的消费者分别取出数据,通过SendToES()将数据发往ES
	err = kafka.Init([]string{cfg.KafkaCfg.Address}, cfg.KafkaCfg.Topic)
	if err != nil {
		fmt.Println("setup kafka consumer failed err ", err)
		return
	}
	select {}
}
