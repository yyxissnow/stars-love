package etcd

import (
	"context"
	"fmt"
	clientv3 "go.etcd.io/etcd/client/v3"
	"log"
	"stars/conn/config"
	"stars/util/common"
	"time"
)

var client *clientv3.Client

func Init(addr string, timeout int64) {
	c, err := clientv3.New(clientv3.Config{
		Endpoints:   []string{addr},
		DialTimeout: time.Duration(timeout) * time.Second,
	})
	if err != nil {
		log.Panic(fmt.Sprintf("ercd init err:%v", err))
		return
	}
	client = c
}

func RegisterEtcd(app, addr string) {
	rand := common.RandString(10)
	lease := clientv3.NewLease(client)
	leaseRes, err := lease.Grant(context.TODO(), 20)
	if err != nil {
		log.Panicf("lease init err:%v", err)
		return
	}
	ctx, cancel := context.WithTimeout(context.TODO(), time.Second*3)
	defer cancel()
	if _, err := client.Put(ctx, config.GetCfg().EtcdConf.Path+app+"."+rand, "http://"+addr, clientv3.WithLease(leaseRes.ID)); err != nil {
		return
	}
	leaseChan, err := lease.KeepAlive(context.TODO(), leaseRes.ID)
	if err != nil {
		log.Panicf("lease keep alive err:%v", err)
		return
	}
	go ListenLeaseRespChan(leaseChan)
}

func ListenLeaseRespChan(leaseChan <-chan *clientv3.LeaseKeepAliveResponse) {
	for leaseKeepResp := range leaseChan {
		leaseKeepResp.String()
	}
	log.Println("关闭续租")
}

func GetEtcdCli() *clientv3.Client {
	return client
}
