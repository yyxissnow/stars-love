package config

import (
	"fmt"
	"gopkg.in/ini.v1"
	"stars/log"
)

var cfg *WebConf

func InitConf() *WebConf {
	cfg = new(WebConf)
	err := ini.MapTo(cfg, "config.ini")
	if err != nil {
		log.Logger.Panic(fmt.Sprintf("setup config err : %v", err))
	}
	return cfg
}

func GetCfg() *WebConf {
	return cfg
}

type WebConf struct {
	Basic      `ini:"basic"`
	MysqlConf  `ini:"mysql"`
	ServerConf `ini:"server"`
	EtcdConf   `ini:"etcd"`
	RedisConf  `ini:"redis"`
	OSSConf    `ini:"oss"`
}

type Basic struct {
	Address string `ini:"address"`
	Run     string `ini:"run"`
}

type MysqlConf struct {
	Dsn         string `ini:"dsn"`
	Database    string `ini:"database"`
	MaxIdleConn int    `ini:"max_idle_conn"`
	MaxOpenConn int    `ini:"max_open_conn"`
}

type ServerConf struct {
	AccountApiAddr     string `ini:"account_api_addr"`
	AccountRpcAddr     string `ini:"account_rpc_addr"`
	LoverApiAddr       string `ini:"lover_api_addr"`
	LoverRpcAddr       string `ini:"lover_rpc_addr"`
	MessageApiAddr     string `ini:"message_api_addr"`
	MessageRpcAddr     string `ini:"message_rpc_addr"`
	WishListApiAddr    string `ini:"wish_list_api_addr"`
	ReminderApiAddr    string `ini:"reminder_api_addr"`
	MarkDayApiAddr     string `ini:"mark_day_api_addr"`
	PrivateChatApiAddr string `ini:"private_chat_api_addr"`
	QuestionAPIAddr    string `ini:"question_api_addr"`
	CommentApiAddr     string `ini:"comment_api_addr"`
	CommentRpcAddr     string `ini:"comment_rpc_addr"`
	LikeApiAddr        string `ini:"like_api_addr"`
	LikeRpcAddr        string `ini:"like_rpc_addr"`
	ArticleApiAddr     string `ini:"article_api_addr"`
	ArticleRpcAddr     string `ini:"article_rpc_addr"`
}

type EtcdConf struct {
	Address string `ini:"address"`
	Timeout int64  `ini:"timeout"`
	Path    string `ini:"path"`
}

type RedisConf struct {
	Address  string `ini:"address"`
	Password string `ini:"password"`
}

type OSSConf struct {
	OSSBucket          string `ini:"oss_bucket"`
	OSSEndpoint        string `ini:"oss_endpoint"`
	OSSAccessKeyId     string `ini:"oss_access_key_id"`
	OSSAccessKeySecret string `ini:"oss_access_key_secret"`
}
