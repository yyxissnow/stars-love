package redis

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"main.go/conn/config"
	"main.go/log"
	"time"
)

var rdb *redis.Client

func InitRedisCli(cfg config.RedisConf) {
	rdb = redis.NewClient(&redis.Options{
		Addr:     cfg.Address,
		Password: cfg.Password,
		DB:       0,
		PoolSize: 100,
	})

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	_, err := rdb.Ping(ctx).Result()
	if err != nil {
		log.Logger.Panic(fmt.Sprintf("setup db err : %v", err))
	}
}

func GetRedisCli() *redis.Client {
	return rdb
}
