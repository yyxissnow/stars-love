package db

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"stars/conn/config"
	"stars/log"
)

var mysqldb *gorm.DB

func InitDB() {
	dsn := fmt.Sprintf(config.GetCfg().Dsn, config.GetCfg().Database)

	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Logger.Panic(fmt.Sprintf("setup db err : %v", err))
	}
	sql, err := db.DB()
	if err != nil {
		log.Logger.Panic(fmt.Sprintf("setup db sql err : %v", err))
	}
	sql.SetMaxIdleConns(config.GetCfg().MaxIdleConn)
	sql.SetMaxOpenConns(config.GetCfg().MaxOpenConn)

	mysqldb = db
}

func GetMysqlDB() *gorm.DB {
	return mysqldb
}
