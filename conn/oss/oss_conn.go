package oss

import (
	"fmt"
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"mime/multipart"
	"stars/conn/config"
)

var ossCli *oss.Client

func Client() *oss.Client {
	if ossCli != nil {
		return ossCli
	}
	ossCli, err := oss.New(config.GetCfg().OSSEndpoint, config.GetCfg().OSSAccessKeyId, config.GetCfg().OSSAccessKeySecret)
	if err != nil {
		fmt.Println(err.Error())
		return nil
	}
	return ossCli
}

//Bucket:获取bucket存储空间
func Bucket() *oss.Bucket {
	cli := Client()
	if cli != nil {
		bucket, err := cli.Bucket(config.GetCfg().OSSBucket)
		if err != nil {
			fmt.Println(err.Error())
			return nil
		}
		return bucket
	}
	return nil
}

//临时授权下载url
func DownloadUrl(objName string) string {
	signedUrl, err := Bucket().SignURL(objName, oss.HTTPGet, 3600)
	if err != nil {
		fmt.Println(err.Error())
		return ""
	}
	return signedUrl
}

func UploadImg(file *multipart.FileHeader, name string) error {
	src, err := file.Open()
	if err != nil {
		return err
	}
	defer src.Close()
	option := oss.ContentType("image/jpg")
	err = Bucket().PutObject(name, src, option)
	if err != nil {
		return err
	}
	return nil
}
